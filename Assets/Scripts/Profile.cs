﻿using System;
using System.Collections;
using System.Collections.Generic;
using Dialogue;
using TMPro;
using UnityEditor;
using UnityEngine;

public class Profile : MonoBehaviour
{
    public static Profile activeProfile = null;
    public string heroName = "Hero";
    public Language language => GetLanguage();
    public string _time = "12:00";
    public int _day = 1;
    public int DayCounter = 0;
    public GameList gameList;
    [NonSerialized] public string nextScene = null;
    public List<FlagVariable> FlagVariables;
    public List<IntVariable> intVariables;
    public List<InventoryObjectScriptableCnt> Items;
    public List<StringVariable> stringVariables = new List<StringVariable>();
    private bool isLoadSave = false;
    public List<LogLine> LogPhrases;


    public void SaveDataNull()
    {
        isLoadSave = false;
    }

    Data LastSave = null;

    public void SetLastSaveData(Data d)
    {
        LastSave = new Data();

        LastSave.heroName = d.heroName;
        heroName = d.heroName;
        _time = d.time;
        _day = d.day;
        LastSave.time = d.time;
        LastSave.day = d.day;
        LastSave.scene = d.scene;

        LastSave.reputation = d.reputation;

        DayCounter = d.DayCounter;
        LastSave.DayCounter = d.DayCounter;

        Items.Clear();
        LastSave.Items.Clear();
        Items = new List<InventoryObjectScriptableCnt>();
        foreach (var item in d.Items)
        {
            InventoryObjectScriptable tmp = new InventoryObjectScriptable();
            var itemToList = gameList.inventoryObjects
                .Find(x => x.name == item.CodeName);

            tmp.name = item.CodeName;
            tmp.Name = itemToList.Name;
            tmp.Description = itemToList.Description;

            tmp.Image = itemToList.Image;
            tmp.InteractTags = itemToList.InteractTags;
            tmp.InteractGraph = itemToList.InteractGraph;
            tmp.interactName = itemToList.interactName;
            Items.Add(new InventoryObjectScriptableCnt(tmp, item.cnt));
        }

        LastSave.Items = new List<InventoryObjectForSave>(d.Items);

        LastSave.intVariables.Clear();
        LastSave.intVariables = new List<IntVariable>(d.intVariables);
        intVariables.Clear();
        intVariables = new List<IntVariable>(d.intVariables);

        LastSave.stringVariables.Clear();
        LastSave.stringVariables = new List<StringVariable>(d.stringVariables);
        stringVariables.Clear();
        stringVariables = new List<StringVariable>(d.stringVariables);
        LastSave.FlagVariables.Clear();
        LastSave.FlagVariables = new List<FlagVariable>(d.FlagVariables);
        FlagVariables.Clear();
        FlagVariables = new List<FlagVariable>(d.FlagVariables);
        LastSave.LogLines.Clear();
        LastSave.LogLines = new List<LogLine>(d.LogLines);
        LogPhrases.Clear();
        LogPhrases = new List<LogLine>(d.LogLines);
        LastSave.SavedCharactersInfos.Clear();
        LastSave.SavedCharactersInfos = new List<SavedCharactersInfo>(d.SavedCharactersInfos);
        LastSave.currentSoundsInSounders.Clear();
        LastSave.currentSoundsInSounders = new List<string>(d.currentSoundsInSounders);
        LastSave.nodeIndex = d.nodeIndex;
        Debug.Log(LastSave.nodeIndex);
        LastSave.DialogueLineIndex = d.DialogueLineIndex;
        Debug.Log(LastSave.DialogueLineIndex);
        LastSave.GraphPath = d.GraphPath;
        Debug.Log(LastSave.GraphPath);

        LastSave.ImageObjects = d.ImageObjects;

        isLoadSave = true;
        var cd = FindObjectOfType<CommandDebugger>();
        if (cd != null)
        {
            cd.UpdateFlagsList(this);
            cd.UpdateVarsList(this);
        }
    }

    public void SetIsLoadSave(bool b)
    {
        isLoadSave = b;
    }

    public bool GetIsLoadSave() => isLoadSave;

    [HideInInspector]
    public int Sound_General_sound
    {
        get
        {
            if (!PlayerPrefs.HasKey("Sound_General_sound"))
                PlayerPrefs.SetInt("Sound_General_sound", 50);
            return PlayerPrefs.GetInt("Sound_General_sound");
        }
        set { PlayerPrefs.SetInt("Sound_General_sound", value); }
    }

    [HideInInspector]
    public int Sound_Music
    {
        get
        {
            if (!PlayerPrefs.HasKey("Sound_Music"))
                PlayerPrefs.SetInt("Sound_Music", 50);
            return PlayerPrefs.GetInt("Sound_Music");
        }
        set { PlayerPrefs.SetInt("Sound_Music", value); }
    }

    [HideInInspector]
    public int Sound_Effects
    {
        get
        {
            if (!PlayerPrefs.HasKey("Sound_Effects"))
                PlayerPrefs.SetInt("Sound_Effects", 50);
            return PlayerPrefs.GetInt("Sound_Effects");
        }
        set { PlayerPrefs.SetInt("Sound_Effects", value); }
    }

    //************************************************************
    public void Awake()
    {
        DontDestroyOnLoad(this);

        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
            return;
        }

        activeProfile = this;

        if (PlayerPrefs.HasKey("LangIndex"))
            SetLanguage(PlayerPrefs.GetInt("LangIndex"));
        else
            SetLanguage(0);

        if (PlayerPrefs.HasKey("ResIndex") && PlayerPrefs.HasKey("ModeIndex"))
        {
/*            defaultResolution = PlayerPrefs.GetInt("ResIndex");
            defaultMode = PlayerPrefs.GetInt("ModeIndex");*/

            Sound_General_sound = PlayerPrefs.GetInt("Sound_General_sound");
            Sound_Effects = PlayerPrefs.GetInt("Sound_Effects");
            Sound_Music = PlayerPrefs.GetInt("Sound_Music");
        }

#if UNITY_EDITOR
        var gl = Instantiate(gameList);
        var sprites = Resources.LoadAll<Sprite>("2d");

        bool flag = false;
        foreach (var sprt in sprites)
        {
            if (!gl.sprites.Contains(sprt))
            {
                gl.sprites.Add(sprt);
                flag = true;
            }
        }

        if (flag)
            PrefabUtility.ReplacePrefab(gl.gameObject, gameList, ReplacePrefabOptions.ReplaceNameBased);

        Destroy(gl);
#endif
    }

    public void DialogueStartWaiting(Wait wait, float time)
    {
        StartCoroutine(Waiting(wait, time));
    }

    IEnumerator Waiting(Wait wait, float time)
    {
        yield return new WaitForSeconds(time);
        wait.TriggerNext();
    }

    //**************************************************************
    public bool isVarExist(string name)
    {
        foreach (IntVariable cr in intVariables)
        {
            if (cr.name == null) cr.name = "";
            if (cr.name.Equals(name))
            {
                return true;
            }
        }

        return false;
    }

    public bool isVarGreater(string name, int rep)
    {
        if (!isVarExist(name))
        {
            return false;
        }

        var index = intVariables.FindIndex(c => c.name.Equals(name));

        return intVariables[index].value > rep;
    }

    public bool isVarEqual(string name, int rep)
    {
        if (!isVarExist(name))
        {
            return false;
        }

        var index = intVariables.FindIndex(c => c.name.Equals(name));

        return intVariables[index].value == rep;
    }

    public bool isVarLess(string name, int rep)
    {
        if (!isVarExist(name))
        {
            return false;
            ;
        }

        var index = intVariables.FindIndex(c => c.name.Equals(name));

        return intVariables[index].value < rep;
    }

    public bool isItemExist(string codeName)
    {
        foreach (InventoryObjectScriptableCnt io in Items)
        {
            if (io.inventoryObjectScriptable.name.Equals(codeName))
            {
                return true;
            }
        }

        return false;
    }

    public bool isStringExist(string param)
    {
        foreach (StringVariable sv in stringVariables)
        {
            if (sv.name.Equals(param))
            {
                return true;
            }
        }

        return false;
    }

    public bool isFlagExist(string name)
    {
        foreach (FlagVariable flag in FlagVariables)
        {
            if (flag.name.Equals(name))
            {
                return true;
            }
        }

        return false;
    }

    public bool IsFlagRaised(string name)
    {
        foreach (FlagVariable flag in FlagVariables)
        {
            if (flag.name.Equals(name))
            {
                return flag.isRaised;
            }
        }

        return false;
    }

    //*****************************************************
    public void AddLogLine(LogLine l)
    {
        LogPhrases.Add(l);
#if UNITY_ANDROID
        if (LogPhrases.Count > 25)
            LogPhrases.RemoveAt(0);

#endif
        if (LogPhrases.Count > 100)
            LogPhrases.RemoveAt(0);
    }

    public void CreateVar(string name)
    {
        if (!isVarExist(name))
        {
            IntVariable cr = new IntVariable(name, 0);
            intVariables.Add(cr);
        }

        var deb = FindObjectOfType<CommandDebugger>();
        if (deb != null) deb.UpdateVarsList(this);
    }

    public void AddVar(string name, int rep)
    {
        var index = intVariables.FindIndex(c => c.name.Equals(name));
        if (index == -1)
        {
            IntVariable cr = new IntVariable(name, rep);
            intVariables.Add(cr);
        }
        else
            intVariables[index] = new IntVariable(name, intVariables[index].value + rep);

        FindObjectOfType<CommandDebugger>().UpdateVarsList(this);
    }

    public void SetVar(string name, int rep)
    {
        var index = intVariables.FindIndex(c => c.name.Equals(name));
        if (index == -1)
        {
            IntVariable cr = new IntVariable(name, rep);
            intVariables.Add(cr);
        }
        else
            intVariables[index] = new IntVariable(name, rep);

        FindObjectOfType<CommandDebugger>().UpdateVarsList(this);
    }

    public void RemoveVar(string name, int rep)
    {
        var index = intVariables.FindIndex(c => c.name.Equals(name));
        if (index == -1)
        {
            IntVariable cr = new IntVariable(name, -rep);
            intVariables.Add(cr);
        }
        else
            intVariables[index] = new IntVariable(name, intVariables[index].value - rep);

        var deb = FindObjectOfType<CommandDebugger>();
        if (deb != null) deb.UpdateVarsList(this);
    }

    public void DeleteVar(string param)
    {
        var index = intVariables.FindIndex(c => c.name.Equals(param));
        if (index >= 0)
            intVariables.RemoveAt(index);
        FindObjectOfType<CommandDebugger>().UpdateVarsList(this);
    }

    public void RemoveItem(string itemName)
    {
        var index = Items.FindIndex(c => c.inventoryObjectScriptable.name.Equals(itemName));
        if (index >= 0)
        {
            if (Items[index].count <= 1)
                Items.RemoveAt(index);
            else
            {
                Items[index] =
                    new InventoryObjectScriptableCnt(Items[index].inventoryObjectScriptable, Items[index].count - 1);
            }
        }
    }

    public void ReplaceItem(string itemName, InventoryObjectScriptable item)
    {
        var index = Items.FindIndex(c => c.inventoryObjectScriptable.name.Equals(itemName));
        if (index >= 0)
        {
            Items[index] = new InventoryObjectScriptableCnt(item, item.isCountable ? 1 : 0);
        }
    }

    public void AddItem(InventoryObjectScriptable item)
    {
        if (!isItemExist(item.name))
        {
            Items.Add(new InventoryObjectScriptableCnt(item, item.isCountable ? 1 : 0));
        }
        else
        {
            if (item.isCountable)
            {
                var index = Items.FindIndex(c => c.inventoryObjectScriptable.Equals(item));
                Items[index] = new InventoryObjectScriptableCnt(item, Items[index].count + 1);
            }
        }
    }

    public void CreateStringVar(string key, string value)
    {
        if (!isStringExist(key))
        {
            StringVariable tmp = new StringVariable(key, value);
            stringVariables.Add(tmp);
        }
    }

    public void SetStringVar(string key, string value)
    {
        var index = stringVariables.FindIndex(c => c.name.Equals(key));
        if (index == -1)
        {
            StringVariable cr = new StringVariable(key, value);
            stringVariables.Add(cr);
        }
        else
            stringVariables[index] = new StringVariable(name, value);
    }

    public void DeleteStringVar(string key)
    {
        var index = stringVariables.FindIndex(c => c.name.Equals(key));
        if (index >= 0)
        {
            stringVariables.RemoveAt(index);
        }
    }

    public void AddFlag(string name)
    {
        if (!isFlagExist(name))
        {
            FlagVariable flag = new FlagVariable(name, false);
            FlagVariables.Add(flag);
        }

        FindObjectOfType<CommandDebugger>().UpdateFlagsList(this);
    }

    public void RemoveFlag(string Name)
    {
        var index = FlagVariables.FindIndex(c => c.name.Equals(Name));
        if (index >= 0)
            FlagVariables.RemoveAt(index);

        FindObjectOfType<CommandDebugger>().UpdateFlagsList(this);
    }

    public void RiseFlag(string name)
    {
        var index = FlagVariables.FindIndex(c => c.name.Equals(name));
        if (index == -1)
        {
            FlagVariable flag = new FlagVariable(name, true);
            FlagVariables.Add(flag);
        }
        else
            FlagVariables[index] = new FlagVariable(name, true);

        FindObjectOfType<CommandDebugger>().UpdateFlagsList(this);
    }

    public void LowerFlag(string name)
    {
        var index = FlagVariables.FindIndex(c => c.name.Equals(name));
        if (index == -1)
        {
            FlagVariable flag = new FlagVariable(name, false);
            FlagVariables.Add(flag);
        }
        else
            FlagVariables[index] = new FlagVariable(name, false);

        var deb = FindObjectOfType<CommandDebugger>();
        if (deb != null) deb.UpdateFlagsList(this);
    }

    public void SetLanguage(int i)
    {
        PlayerPrefs.SetInt("LangIndex", i);
    }

    public Language GetLanguage()
    {
        return (Language) PlayerPrefs.GetInt("LangIndex");
    }

    public int GetVar(string name)
    {
        var index = intVariables.FindIndex(c => c.name.Equals(name));
        if (index >= 0)
            return intVariables[index].value;
        return 0;
    }

    public string GetStringVar(string name)
    {
        var index = stringVariables.FindIndex(c => c.name.Equals(name));
        if (index >= 0)
            return stringVariables[index].value;
        return "";
    }
}