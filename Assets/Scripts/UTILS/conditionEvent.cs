using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class conditionEvent : MonoBehaviour
{
    public List<Dialogue.Condition> conditions;

    public UnityEvent trueEvent;
    public UnityEvent falseEvent;

    public void TrueEventInvoke()
    {
        trueEvent.Invoke();
    }

    public void FalseEventInvoke()
    {
        falseEvent.Invoke();
    }

    public void Invoke()
    {
        foreach (var c in conditions)
        {
            if (!c.Invoke())
            {
                FalseEventInvoke();
                return;
            }
        }

        TrueEventInvoke();
    }
}