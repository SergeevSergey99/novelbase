using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoad : MonoBehaviour
{
    public void Load(string scene)
    {
        SceneManager.LoadScene(scene, LoadSceneMode.Single);
    }

    public void LoadAfterSecond(string scene)
    {
        StartCoroutine(LoadingAfter(scene, 1));
    }

    public void LoadAfter2Seconds(string scene)
    {
        StartCoroutine(LoadingAfter(scene, 2));
    }

    IEnumerator LoadingAfter(string scene, float time)
    {
        yield return new WaitForSeconds(time);
        SceneManager.LoadScene(scene, LoadSceneMode.Single);

    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public bool isPlayerPrefsSet(string key)
    {
        return PlayerPrefs.HasKey(key);
    }
    public void SetPlayerPrefsKeyVal(string keyval)
    {
        var strs = keyval.Split(",");
        PlayerPrefs.SetString(strs[0], strs[1]);
    }
    public void DeletePlayerPrefsKeyVal(string key)
    {
        PlayerPrefs.DeleteKey(key);
    }
    private AsyncOperation asyncLoad = null;
    public void StartAsyncLoad(string scene)
    {
        asyncLoad = SceneManager.LoadSceneAsync(scene);
        asyncLoad.allowSceneActivation = false;
    }

    public void GoToAsyncScene()
    {
        asyncLoad.allowSceneActivation = true;
    }
    
}
