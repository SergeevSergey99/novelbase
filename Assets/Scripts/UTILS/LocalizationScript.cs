﻿
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LocalizationScript : MonoBehaviour
{
    [TextArea]
    public String RU_Str = "";
    public float RU_AddFontSize = 0;
    [TextArea]
    public String EN_Str = "";
    public float EN_AddFontSize = 0;

    private float fontSize = 0;

    public void SetRu(string val)
    {
        RU_Str = val;
    }
    public void SetEn(string val)
    {
        EN_Str = val;
    }

    private void Awake()
    {
        
        if(GetComponent<Text>())
            fontSize = GetComponent<Text>().fontSize;
        if(GetComponent<TextMeshPro>())
            fontSize = GetComponent<TextMeshPro>().fontSize;
        if(GetComponent<TextMeshProUGUI>())
            fontSize = GetComponent<TextMeshProUGUI>().fontSize;
    }
    private void OnEnable()
    {
        var lang = (Language)PlayerPrefs.GetInt("LangIndex", 0);
	    if(fontSize == 0) Awake();
	    String _RU = DLineFormatter.Format(RU_Str);
	    String _EN = DLineFormatter.Format(EN_Str);
        
        if (lang == Language.RU)
        {
            if (GetComponent<Text>())
            {
                GetComponent<Text>().text = _RU;
                GetComponent<Text>().fontSize = (int)(fontSize + RU_AddFontSize);
            }
            if (GetComponent<TextMeshPro>())
            {
                GetComponent<TextMeshPro>().text = _RU;
                GetComponent<TextMeshPro>().fontSize = fontSize + RU_AddFontSize;
            }
            if (GetComponent<TextMeshProUGUI>())
            {
                GetComponent<TextMeshProUGUI>().text = _RU;
                GetComponent<TextMeshProUGUI>().fontSize = fontSize + RU_AddFontSize;
            }

        }
        else if (lang == Language.EN)
        {

            if (GetComponent<Text>())
            {
                GetComponent<Text>().text = _EN;
                GetComponent<Text>().fontSize = (int)(fontSize + EN_AddFontSize);
            }

            if (GetComponent<TextMeshProUGUI>())
            {
                GetComponent<TextMeshProUGUI>().text = _EN;
                GetComponent<TextMeshProUGUI>().fontSize = fontSize + EN_AddFontSize;
            }

            if (GetComponent<TextMeshPro>())
            {
                GetComponent<TextMeshPro>().text = _EN;
                GetComponent<TextMeshPro>().fontSize = fontSize + EN_AddFontSize;
                
            }
        }
    }

    public void Localize() => OnEnable();

}