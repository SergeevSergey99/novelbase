using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ClearProfile : MonoBehaviour
{
    public UnityEvent IsDevEvent;
    public UnityEvent IsNotDevEvent;
    
    // Start is called before the first frame update
    void Start()
    {
        Profile prf = GameObject.Find("Profile").GetComponent<Profile>(); 
        prf.heroName = "Hero";
        prf._time = "12:00";
        prf._day = 1;
        prf.DayCounter = 0;
        
        prf.FlagVariables.Clear();
        prf.intVariables.Clear();
        prf.Items.Clear();
        prf.stringVariables.Clear();
        prf.LogPhrases.Clear();
        prf.SaveDataNull();

#if UNITY_ANDROID
        
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
#endif
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        IsDevEvent.Invoke();
#else
        IsNotDevEvent.Invoke();
#endif
    }

    
}
