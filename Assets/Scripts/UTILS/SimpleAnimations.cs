using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent (typeof (CanvasGroup))]
public class SimpleAnimations : MonoBehaviour
{
    private CanvasGroup cg => GetComponent<CanvasGroup>();
    [SerializeField] private float animTime = .5f;
    private float startX = 0;
    private float startY = 0;
    public float offset = 100f;
    private int id = -1, id2 = -1;

    [NonSerialized]public float nextAlpha;
    [NonSerialized]public bool nextInteractable;
    [NonSerialized]public bool nextBlockRaycast;

    private Coroutine cor = null;
    public float GetAnimTime() => animTime;
   /* public float GetNextAlpha()
    {
        return nextAlpha;
    }*/
    public float GetStartX()
    {
        return startX;
    }

    public void setAnimTime(float t)
    {
        animTime = t;
    }
    private bool seted = false;
    private void Awake()
    {
        if(seted) return;
        seted = true;
        nextAlpha = cg.alpha;
        nextInteractable = cg.interactable;
        nextBlockRaycast = cg.blocksRaycasts;
        
        startX = transform.localPosition.x;
        startY = transform.localPosition.y;
    }

    public void AppearAnim()
    {
        
        cg.blocksRaycasts = true;
        cg.interactable = true;
        nextAlpha = 1;
        nextInteractable = true;
        nextBlockRaycast = true;
        if (cg.alpha >= 0.995 && !LeanTween.isTweening(id) && !LeanTween.isTweening(id2)) return;
        Awake();
        
        LeanTween.cancel(id);
        LeanTween.cancel(id2);
        LeanTween.moveLocalX(gameObject, startX + offset, 0);
        id = LeanTween.moveLocalX(gameObject, startX, animTime).setEaseInOutQuad().id;
        id2 = LeanTween.alphaCanvas(cg, 1, animTime).setEaseInOutQuad().id;
    }
    public void AppearAnimZero()
    {
        
        cg.blocksRaycasts = true;
        cg.interactable = true;
        nextAlpha = 1;
        nextInteractable = true;
        nextBlockRaycast = true;
        if (cg.alpha >= 0.995 && !LeanTween.isTweening(id) && !LeanTween.isTweening(id2)) return;
        Awake();
        
        LeanTween.cancel(id);
        LeanTween.cancel(id2);
        id2 = LeanTween.alphaCanvas(cg, 1, animTime).setEaseInOutQuad().id;
    }
    public void ActiveAndAppear()
    {
        if (corActiv != null) StopCoroutine(corActiv);
        gameObject.SetActive(true);
        AppearAnim();
    }
    public void ActiveAndAppearZero()
    {
        gameObject.SetActive(true);
        AppearAnimZero();
    }
    public void ActiveAndAppearAfter(float time)
    {
        gameObject.SetActive(true);
        AppearAfter(time);
    }
    public void ActiveAndAppearYAfter(float time = -1)
    {
        gameObject.SetActive(true);
        if(time < 0)
            AppearYAfter(animTime);
        AppearYAfter(time);
    }
    public void ActiveAndAppearY()
    {
        gameObject.SetActive(true);
        AppearAnimY();
    }
    public void AppearAnimY()
    {
        if (cg.alpha >= 0.995 && !LeanTween.isTweening(id) && !LeanTween.isTweening(id2)) return;
        
        cg.blocksRaycasts = true;
        cg.interactable = true;
        Awake();
        
        nextAlpha = 1;
        nextInteractable = true;
        nextBlockRaycast = true;
        LeanTween.cancel(id);
        LeanTween.cancel(id2);
        LeanTween.moveLocalY(gameObject, startY + offset, 0);
        id = LeanTween.moveLocalY(gameObject, startY, animTime).setEaseInOutQuad().id;
        id2 = LeanTween.alphaCanvas(cg, 1, animTime).setEaseInOutQuad().id;
        
    }
    public void AppearAfter(float time)
    {
        nextAlpha = 1;
        nextBlockRaycast = true;
        nextInteractable = true;
        if(cor != null) StopCoroutine(cor); 
        cor = StartCoroutine(AppearingAfter(time));
    }
    public void AppearYAfter(float time)
    {
        nextAlpha = 1;
        nextBlockRaycast = true;
        nextInteractable = true;
        if(corY != null) StopCoroutine(corY); 
        corY = StartCoroutine(AppearingYAfter(time));
    }
    public void DisappearAfter(float time)
    {
        nextAlpha = 0;
        nextBlockRaycast = false;
        nextInteractable = false;
        if(cor != null) StopCoroutine(cor); 
        cor = StartCoroutine(DisappearingAfter(time));
    }
    public void AppearNotActiveAfter(float time)
    {
        
        nextAlpha = 1;
        nextInteractable = false;
        nextBlockRaycast = true;
        if(cor != null) StopCoroutine(cor); 
        cor = StartCoroutine(AppearingNotActiveAfter(time));
    }

    IEnumerator AppearingAfter(float time)
    {
        yield return new WaitForSeconds(time);
        AppearAnim();
    }
    IEnumerator AppearingYAfter(float time)
    {
        yield return new WaitForSeconds(time);
        AppearAnimY();
    }
    IEnumerator DisappearingAfter(float time)
    {
        yield return new WaitForSeconds(time);
        DissapearAnim();
    }
    IEnumerator AppearingNotActiveAfter(float time)
    {
        yield return new WaitForSeconds(time);
        AppearAnimNotActive();
    }
    public void AppearAnimNotActive()
    {
        cg.blocksRaycasts = true;
        Awake();
        LeanTween.cancel(id);
        LeanTween.cancel(id2);
        LeanTween.moveLocalX(gameObject, startX + offset, 0);
        id = LeanTween.moveLocalX(gameObject, startX, animTime).setEaseInOutQuad().id;
        id2 = LeanTween.alphaCanvas(cg, 1, animTime).setEaseInOutQuad().id;
        
        nextAlpha = 1;
        nextInteractable = false;
        nextBlockRaycast = true;
    }

    private Coroutine corY = null;
    private Coroutine corActiv = null;
    public void DissapearYAndDeactive()
    {
        if(!gameObject.activeSelf) return;
        DissapearAnimY();
        if(corActiv != null) StopCoroutine(corY); 
        corActiv = StartCoroutine(SetActiveAfter(false, animTime));
    }
    public void DissapearAndDeactive()
    {
        if(gameObject.activeSelf == false) return;
        DissapearAnim();
        if(corActiv != null) StopCoroutine(cor); 
        corActiv = StartCoroutine(SetActiveAfter(false, animTime*1.01f));
    }
    public void DissapearAndDeactiveZero()
    {
        if(gameObject.activeSelf == false) return;
        DissapearZeroAnim();
        if(corActiv != null) StopCoroutine(cor); 
        corActiv = StartCoroutine(SetActiveAfter(false, animTime*1.01f));
    }
    public void DissapearAndDestroy()
    {
        if(gameObject.activeSelf == false) return;
        DissapearAnim();
        if(corActiv != null) StopCoroutine(cor); 
        corActiv = StartCoroutine(DestroyAfter(animTime*1.01f));
    }

    public void Deactive()
    {
        if(gameObject.activeSelf == false) return;
        if (cg.alpha <= 0.005 && (!LeanTween.isTweening(id)) && !LeanTween.isTweening(id2)) return;
        cg.blocksRaycasts = false;
        cg.interactable = false;
        Awake();
        LeanTween.cancel(id);
        LeanTween.cancel(id2);
        id = LeanTween.moveLocalX(gameObject, startX, 0).setEaseInOutQuad().id;
        id2 = LeanTween.alphaCanvas(cg, 0, 0).setEaseInOutQuad().id;
        nextAlpha = 0;
        nextBlockRaycast = false;
        nextInteractable = false;
        
        gameObject.SetActive(false);
    }

    public void DissapearAnim()
    {
        if(gameObject.activeSelf == false) return;
        if (cg.alpha <= 0.005 && !LeanTween.isTweening(id) && !LeanTween.isTweening(id2)) return;
        cg.blocksRaycasts = false;
        cg.interactable = false;
        Awake();
        LeanTween.cancel(id);
        LeanTween.cancel(id2);
        id = LeanTween.moveLocalX(gameObject, startX + offset, animTime).setEaseInOutQuad().id;
        id2 = LeanTween.alphaCanvas(cg, 0, animTime).setEaseInOutQuad().id;
        nextAlpha = 0;
        nextInteractable = false;
        nextBlockRaycast = false;
        if(cor != null) StopCoroutine(cor); 
        cor = StartCoroutine(SetToAfter(startX, animTime));
    }
    public void DissapearZeroAnim()
    {
        if(gameObject.activeSelf == false) return;
        if (cg.alpha <= 0.005 && !LeanTween.isTweening(id) && !LeanTween.isTweening(id2)) return;
        cg.blocksRaycasts = false;
        cg.interactable = false;
        Awake();
        LeanTween.cancel(id);
        LeanTween.cancel(id2);
        id2 = LeanTween.alphaCanvas(cg, 0, animTime).setEaseInOutQuad().id;
        nextAlpha = 0;
        nextInteractable = false;
        nextBlockRaycast = false;
        if(cor != null) StopCoroutine(cor); 
    }
    public void DissapearAnimY()
    {
        if (cg.alpha <= 0.005 && !LeanTween.isTweening(id) && !LeanTween.isTweening(id2)) return;
        cg.blocksRaycasts = false;
        cg.interactable = false;
        Awake();
        LeanTween.cancel(id);
        LeanTween.cancel(id2);
        id = LeanTween.moveLocalY(gameObject, startY + offset, animTime).setEaseInOutQuad().id;
        id2 = LeanTween.alphaCanvas(cg, 0, animTime).setEaseInOutQuad().id;
        nextAlpha = 0;
        nextBlockRaycast = false;
        nextInteractable = false;
        LeanTween.moveLocalY(gameObject, startY, 0);
        if(corY != null) StopCoroutine(corY); 
        corY = StartCoroutine(SetToAfterY(startY, animTime));
    }
    
    
    IEnumerator SetToAfter(float posX, float time)
    {
        yield return new WaitForSeconds(time);
        LeanTween.moveLocalX(gameObject, posX, 0);
    }
    IEnumerator SetToAfterY(float posY, float time)
    {
        yield return new WaitForSeconds(time);
        LeanTween.moveLocalY(gameObject, posY, 0);
    }

    IEnumerator SetActiveAfter(bool act, float time)
    {
        yield return new WaitForSeconds(time);
        gameObject.SetActive(act);
    }
    IEnumerator DestroyAfter(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
    }
}
