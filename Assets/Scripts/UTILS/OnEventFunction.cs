using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnEventFunction : MonoBehaviour
{

    public List<UnityEvent> onAwakeFunctions;
    public List<UnityEvent> onStartFunctions;
    public List<UnityEvent> onEnableFunctions;
    public List<UnityEvent> onDisableFunctions;
    public List<UnityEvent> onDestroyFunctions;
    public void Awake()
    {
        foreach (var VARIABLE in onAwakeFunctions)
        {
            VARIABLE.Invoke();
        }
    }

    // Start is called before the first frame update
    public void Start()
    {
        foreach (var VARIABLE in onStartFunctions)
        {
            VARIABLE.Invoke();
        }
    }

    public void OnEnable()
    {
        foreach (var VARIABLE in onEnableFunctions)
        {
            VARIABLE.Invoke();
        }
    }

    public void OnDisable()
    {
        foreach (var VARIABLE in onDisableFunctions)
        {
            VARIABLE.Invoke();
        }
    }

    public void OnDestroy()
    {
        foreach (var VARIABLE in onDestroyFunctions)
        {
            VARIABLE.Invoke();
        }
    }
}
