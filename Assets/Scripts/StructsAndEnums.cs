﻿
using System;
using UnityEngine;

[System.Serializable]
public enum Language
{
    EN,
    RU
    
};
[System.Serializable]
public class LocalizedString
{
    public string EN;
    public string RU;

    public string GetLocalisedString(Language lang)
    {
        switch (lang)
        {
            case Language.EN: return DLineFormatter.Format(EN);
            case Language.RU: return DLineFormatter.Format(RU);
        }

        return DLineFormatter.Format(EN);
    }
    
    public string GetLocalisedString(Profile prf)
    {
        switch (prf.language)
        {
            case Language.EN: return DLineFormatter.Format(EN, prf);
            case Language.RU: return DLineFormatter.Format(RU, prf);
        }

        return DLineFormatter.Format(EN, prf);
    }
}
[System.Serializable]
public class LocalizedArea
{
    [TextArea]
    public string EN;
    [TextArea]
    public string RU;
    
    public string GetLocalisedString(Language lang)
    {
        switch (lang)
        {
            case Language.EN: return DLineFormatter.Format(EN);
            case Language.RU: return DLineFormatter.Format(RU);
        }

        return DLineFormatter.Format(EN);
    }
    public string GetLocalisedString(Profile prf)
    {
        switch (prf.language)
        {
            case Language.EN: return DLineFormatter.Format(EN, prf);
            case Language.RU: return DLineFormatter.Format(RU, prf);
        }

        return DLineFormatter.Format(EN, prf);
    }
}
[System.Serializable]
public class LocationNamePair
{
    public Sprite codeName;
    public string naming;
    public string EN_naming;
}
[System.Serializable]
public class ModelNamePair
{
    [SerializeField] private DModel dModel;
    [SerializeField] private string modelName;

    public DModel DModel
    {
        get => dModel;
        set => dModel = value;
    }

    public string ModelName
    {
        get => modelName;
        set => modelName = value;
    }
}
[System.Serializable]
public class TimeIcon
{
    public int HourGreaterEqualThan;
    public int HourLessThan;
    public Sprite Icon;
}

[System.Serializable]
public enum LogType
{
    Chat,
    Choosen,
    Event
};

[System.Serializable]
public class DLines 
{
    public string WhoTalk;
    public string Line_Rus;
    public string Line_Eng;
    public string Tags;
    public string Expressions;
    public string OnEnd;
}
[System.Serializable]
public class LogLine
{
    public LogType type;

    //public CharacterInfo character;
    public Dialogue.CharacterInfo WhoTalk;

    [TextArea] public string text;
    [TextArea] public string textEN;

    public string Tags;
    public string OnEnd;
    public string Expressions;
}

[System.Serializable]
public class FlagVariable
{
    public string name;
    public bool isRaised;

    public FlagVariable()
    {
    }

    public FlagVariable(string n, bool b)
    {
        name = n;
        isRaised = b;
    }
}

[System.Serializable]
public class SceneMemory
{
    public string SceneName;
    public string GraphObject;
    public int NodeIndex;

    public SceneMemory()
    {
    }

    public SceneMemory(string sn, string go, int ni)
    {
        SceneName = sn;
        GraphObject = go;
        NodeIndex = ni;
    }
}
[Serializable]
public class InventoryObjectScriptableCnt
{
    public InventoryObjectScriptableCnt(InventoryObjectScriptable io, int cnt)
    {
        inventoryObjectScriptable = io;
        count = cnt;
    }
    public InventoryObjectScriptable inventoryObjectScriptable;
    public int count;
}
[System.Serializable]
public class IntVariable
{
    public string name;
    public int value;

    public IntVariable()
    {
    }

    public IntVariable(string n, int r)
    {
        name = n;
        value = r;
    }
}

[System.Serializable]
public class StringVariable
{
    public string name;
    public string value;

    public StringVariable()
    {
    }

    public StringVariable(string n, string v)
    {
        name = n;
        value = v;
    }
}
