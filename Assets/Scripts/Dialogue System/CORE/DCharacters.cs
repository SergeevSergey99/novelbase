using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DCharacters : MonoBehaviour
{
    public Transform CharactersArea;
    private Dictionary<string, Dialogue.CharacterInfo> modelsDic = new Dictionary<string, Dialogue.CharacterInfo>();

    private void Awake()
    {
        var modelPairs = FindObjectOfType<Profile>().gameList.GetComponent<GameList>().characterInfos;
        foreach (var ci in modelPairs)
            if (!modelsDic.ContainsKey(ci.name) && ci != null)
                modelsDic.Add(ci.name, ci);
    }

    private Dictionary<string, DModel> modelsInScene = new Dictionary<string, DModel>();

    public DModel AddCharcter(string model)
    {
        if (!Contains(model))
        {
            if (modelsDic[model]?.dModel != null)
            {
                var ch = Instantiate(modelsDic[model].dModel.gameObject, CharactersArea);
                modelsInScene.Add(model, ch.GetComponent<DModel>());
                ch.GetComponent<DModel>().HideModel();
                return ch.GetComponent<DModel>();
            }
            return null;
        }

        return modelsInScene[model];
    }

    public void RemoveCharacter(string model)
    {
        var ch = modelsInScene[model].gameObject;
        modelsInScene.Remove(model);
        Destroy(ch);
    }

    public bool Contains(string model) => modelsInScene.ContainsKey(model);


    public Dictionary<string, DModel> GetModelsInSceneDic() => modelsInScene;

    public DModel GetModel(string key)
    {
        if (modelsInScene.Count == 0 || !modelsInScene.ContainsKey(key)) return null;
        return modelsInScene[key];
    }
}