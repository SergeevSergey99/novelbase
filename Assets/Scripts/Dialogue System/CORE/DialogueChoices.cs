using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using Dialogue;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DialogueChoices : MonoBehaviour
{
    public GameObject choicesPrefab;
    public GameObject choicesLeft;
    public GameObject choicesRight;
    public DialogueController nCore;
    //[SerializeField] List<NodeChoice> choices = new List<NodeChoice>();

    //private List<Chat.Answer> lastAnswers;

    private EventSystem es;

    private void Start()
    {
        es = FindObjectOfType<EventSystem>(true);

    }

    public void InitChoices(List<Chat.Answer> Answers,List<Chat.Answer> AnswersRight, Language lang)
    {
        if (corr != null)
        {
            StopCoroutine(corr);
        }
        corr = StartCoroutine(deactiveAnswers(Answers, AnswersRight,lang));
    }

    public bool isActive()
    {
        return choicesLeft.activeSelf || choicesRight.activeSelf;
    }
    IEnumerator deactiveAnswers(List<Chat.Answer> Answers, List<Chat.Answer> AnswersRight, Language lang)
    {
        if(choicesLeft.activeSelf || choicesRight.activeSelf)
        {
            foreach (Transform VARIABLE in choicesLeft.transform)
            {
                VARIABLE.GetComponent<DialogueChoice>().Dissapear();
            }
            foreach (Transform VARIABLE in choicesRight.transform)
            {
                VARIABLE.GetComponent<DialogueChoice>().Dissapear();
            }

            yield return new WaitForSeconds(choicesPrefab.GetComponent<SimpleAnimations>().GetAnimTime()*1.15f);

            if(es == null) Start();
            if(es != null) es.SetSelectedGameObject(null);

        }

        int i = 0;
        choicesLeft.SetActive(true);
        choicesRight.SetActive(true);
        for ( ;i < Answers.Count; i++)
        {
            bool flag = false;
            if (Answers[i].conditions != null)
            foreach (var condition in Answers[i].conditions)
            {
                if (condition.Invoke() == false)
                {
                    flag = true;
                    break;
                }
            }
            if(flag) continue;

            var choice = Instantiate(choicesPrefab, choicesLeft.transform).GetComponent<DialogueChoice>();
            choice.index = i;

            choice.Init(lang, Answers[i].text, Answers[i].textEN, this, nCore);

            choice.GetComponent<CanvasGroup>().alpha = 0;
            choice.GetComponent<CanvasGroup>().interactable = false;
            choice.Appear();
        }
        for ( ;i - Answers.Count < AnswersRight.Count; i++)
        {
            bool flag = false;
            if (AnswersRight [i-Answers.Count].conditions != null)
            foreach (var condition in AnswersRight[i - Answers.Count].conditions)
            {
                if (condition.Invoke() == false)
                {
                    flag = true;
                    break;
                }
            }
            if(flag) continue;

            var choice = Instantiate(choicesPrefab, choicesRight.transform).GetComponent<DialogueChoice>();
            choice.index = i;

            choice.Init(lang, AnswersRight[i - Answers.Count].text, AnswersRight[i - Answers.Count].textEN, this, nCore);

            choice.GetComponent<CanvasGroup>().alpha = 0;
            choice.GetComponent<CanvasGroup>().interactable = false;
            choice.Appear();
        }

    }
    public void Localize(Language lang)
    {

        foreach (Transform choice in choicesLeft.transform)
        {
            choice.GetComponent<DialogueChoice>().Localize(lang);
        }
        foreach (Transform choice in choicesRight.transform)
        {
            choice.GetComponent<DialogueChoice>().Localize(lang);
        }

    }


    private Coroutine corr = null;
    public void HideChoices()
    {
        foreach (Transform choice in choicesLeft.transform)
        {
            choice.GetComponent<DialogueChoice>().Dissapear();
        }
        foreach (Transform choice in choicesRight.transform)
        {
            choice.GetComponent<DialogueChoice>().Dissapear();
        }

        if(es == null) Start();
        if(es != null) es.SetSelectedGameObject(null);
        if(corr != null) StopCoroutine(corr);
        corr = StartCoroutine(deactive());
    }

    IEnumerator deactive()
    {
        if (choicesLeft.activeSelf || choicesRight.activeSelf)
        {
            if (choicesLeft.transform.childCount + choicesRight.transform.childCount > 0)
            {
                yield return new WaitForSeconds(choicesPrefab.GetComponent<SimpleAnimations>().GetAnimTime() * 1.1f);
                foreach (Transform VARIABLE in choicesLeft.transform)
                {
                    Destroy(VARIABLE.gameObject);
                }

                foreach (Transform VARIABLE in choicesRight.transform)
                {
                    Destroy(VARIABLE.gameObject);
                }
            }

            choicesLeft.SetActive(false);
            choicesRight.SetActive(false);
        }
    }
}
