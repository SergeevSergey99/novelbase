using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DialogueChoice : MonoBehaviour
{
    [SerializeField] DialogueController nCore;
    [SerializeField] DialogueChoices nChoices;
    [SerializeField] SimpleAnimations dChoiceAnims;
    [SerializeField] private TextMeshProUGUI choiceName;
  
    public int index = -1;

    private string text = "";
    private string textEN = "";
    public void Init(Language lang, string line, string lineEn, DialogueChoices ncs, DialogueController nc)
    {
        nChoices = ncs;
        nCore = nc;
        text = line;
        textEN = lineEn;
        Localize(lang);
    }

    public void Localize(Language lang)
    {
        switch (lang)
        {
            case Language.EN: choiceName.text = DLineFormatter.Format(textEN); break;
            case Language.RU: choiceName.text = DLineFormatter.Format(text); break;
        }
    }

    public void InvokeCommand(int i)
    {
        nChoices.HideChoices( );
        nCore.NextAnswer(i);

    }
    public void InvokeCommand()
    {
        nChoices.HideChoices( );
        nCore.NextAnswer(index);

    }

    public void Appear() => dChoiceAnims.ActiveAndAppear();

    public void Dissapear() => dChoiceAnims.DissapearAndDestroy();
}