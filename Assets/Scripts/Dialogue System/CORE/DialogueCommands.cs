using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public class DialogueCommands : MonoBehaviour
{
   
    [SerializeField] private BackGroundAnimation backGroundAnimation;
//    [SerializeField] private DActiveCharacters activeCharacters;
    [SerializeField] private DCharacters dCharacters;
    [SerializeField] private DPositioning dPositioning;
    [SerializeField] private DialogueController dCore;
    [SerializeField] private BackgroundsCtr backgroundsCtr;
    [SerializeField] private DFocusing dFocusing;
    [SerializeField] public List<SoundManager> dSoundManager;
    [SerializeField] private CommandDebugger debugger;
    public CommandDebugger GetDebbuger() => debugger;

        public void EnterCommad(string command)
    {
        command = command.Trim(' ');
        if (command == "" || command == " ") return;
        debugger.AddCommand(command);
        var splitCommand = SplitCommand(command);
        Type thisType = this.GetType();
        string commandName = splitCommand[0];
        MethodInfo theMethod = thisType.GetMethod(commandName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
        object[] parametersArray = new object[] { splitCommand[1] };
        if(theMethod != null)
            theMethod.Invoke(this, parametersArray);
        else
        {
            debugger.AddCommand("<color=#0F0>Wrong command </color>'<color=#F00>" + commandName + "</color>'");
        }
    }

    public void AddDebuggerError(string str)
    {
        debugger.AddCommand(str);
    }

    private string[] SplitCommand(string command)
    {
        try
        {
            
            var strings = command.Split('(');
            strings[1] = strings[1].Trim(')');
            return strings;

        }
        catch (Exception e)
        {
            Debug.Log(command);
        }

        return command.Split('(');
    }
    public void PrintLogColor(string log)
    {
        AddDebuggerError("<color=#0F0>Log '</color>" + log + "<color=#0F0>'</color>");
        PrintLog(log);
    }

    public void AddExp(string param) => AddExp(Int32.Parse(param));


    public DialogueController GetNodeCore() => dCore;

    public void SetProfileTime(string time) => GraphAction.SetProfileTime(time);
    public void SetTime(string time) => GraphAction.SetTime(time);
    public void SetProfileDay(string day) => GraphAction.SetProfileDay(day);
    public void AddProfileDay(string day) => GraphAction.AddProfileDay(day);
    public void SetDay(string day) => GraphAction.SetDay(day);
    public void RemoveItem(string codeName) => GraphAction.RemoveItem(codeName);
    public void AddItem(string codeName) => GraphAction.AddItem(codeName);
    public void ReplaceItem(string codeNames) => GraphAction.ReplaceItem(codeNames);
    IEnumerator Exiting(string param)
    {
        yield return new WaitForSeconds(1);
        if(dCharacters.Contains(param))
            dCharacters.RemoveCharacter(param);
    }
    DModel GetModel(string param)
    {
        var d = dCharacters.GetModel(param);
        if(d == null) GetComponent<DialogueCommands>().AddDebuggerError("<color=#0F0>Wrong charName </color>'<color=#F00>" + param + "</color>'");

        return d;
    }
    public void Enter(string param)
    {
        var parameters = param.Split(',');
        
        DModel model = dCharacters.AddCharcter(parameters[0]);
        model.FadeIn();
        if (parameters.Length > 1) model.AppearIn(float.Parse(parameters[1].Replace('.',',')));
        model.SetTo(dPositioning.GetPos(0));
    }
    public void Exit(string param)
    {
        var parameters = param.Split(',');
        DModel model = GetModel(parameters[0]);
        model.FadeOut();
        if(parameters.Length > 1) model.AppearOut(float.Parse(parameters[1].Replace('.',',')));
        StartCoroutine(Exiting(parameters[0]));
    }
    public void RemoveParameter(string param)
    {
        var parameters = param.Split(',');

        DModel model = GetModel(parameters[0]);
        if(model == null || parameters.Length < 2) return;
        model.RemoveParameter(Int32.Parse(parameters[1]));
    }
    public void RemoveParameterAtIndex(string param)
    {
        var parameters = param.Split(',');

        DModel model = GetModel(parameters[0]);
        if(model == null || parameters.Length < 2) return;
        model.RemoveParameterAtIndex(Int32.Parse(parameters[1]));
    }
    // (string modelName, int index,  float value)
    public void SetParamCONST(string param)
    {
        var parameters = param.Split(',');

        DModel model = GetModel(parameters[0]);
        if(model == null || parameters.Length < 3) return;
        model.SetParamCONST(Int32.Parse(parameters[1]), float.Parse(parameters[2].Replace('.',',')));
    }
    public void SetParamLERP(string param)
    {
        var parameters = param.Split(',');

        DModel model = GetModel(parameters[0]);
        if(model == null || parameters.Length < 3) return;
        model.SetParamLERP(Int32.Parse(parameters[1]), float.Parse(parameters[2].Replace('.',',')));
    }
    public void SetParamSIN(string param)
    {
        var parameters = param.Split(',');

        DModel model = GetModel(parameters[0]);
        if(model == null || parameters.Length < 3) return;
        model.SetParamSIN(Int32.Parse(parameters[1]), float.Parse(parameters[2].Replace('.',',')));
    }
    public void SetParamCOS(string param)
    {
        var parameters = param.Split(',');

        DModel model = GetModel(parameters[0]);
        if(model == null || parameters.Length < 3) return;
        model.SetParamCOS(Int32.Parse(parameters[1]), float.Parse(parameters[2].Replace('.',',')));
    }
    public void SetModelColor(string param)
    {
        var parameters = param.Split(',');

        DModel model = GetModel(parameters[0]);
        var r = float.Parse(parameters[1].Replace(".",",").Trim(' '));
        var g = float.Parse(parameters[2].Replace(".",",").Trim(' '));
        var b = float.Parse(parameters[3].Replace(".",",").Trim(' '));
        float a = 1;
        if(parameters.Length > 4)
           a = float.Parse(parameters[4].Trim(' '));
        model.SetColor(r, g, b, a);
    }
    public void SetModelColorHex(string param)
    {
        var parameters = param.Split(',');

        DModel model = GetModel(parameters[0]);
        Color newCol = new Color();
        parameters[1] = parameters[1].Trim(' ');
        if (!parameters[1].StartsWith("#"))
            parameters[1] = "#" + parameters[1];

        ColorUtility.TryParseHtmlString(parameters[1], out newCol);
        model.SetColor(newCol.r, newCol.g, newCol.b,newCol.a);
    }
    public void MoveTo(string param)
    {
        var parameters = param.Split(',');
        DModel model = GetModel(parameters[0]);
        var pos = dPositioning.GetPos(int.Parse(parameters[1].Trim(' ')));
        model.MoveTo(pos);
    }
    public void MoveAltTo(string param)
    {
        var parameters = param.Split(',');
        DModel model = GetModel(parameters[0]);
        model.MoveTo(dPositioning.GetAltPos(int.Parse(parameters[1].Trim(' '))));
    }
    public void SetPosTo(string param)
    {
        var parameters = param.Split(',');
        DModel model = GetModel(parameters[0]);
        model.SetTo(dPositioning.GetPos(int.Parse(parameters[1].Trim(' '))));
    }
    public void SetAltPosTo(string param)
    {
        var parameters = param.Split(',');
        DModel model = GetModel(parameters[0]);
        model.SetTo(dPositioning.GetAltPos(int.Parse(parameters[1].Trim(' '))));
    }
    public void MirrorTo(string param)
    {
        var parameters = param.Split(',');
        DModel model = GetModel(parameters[0]);
        model.MirrorTo(int.Parse(parameters[1].Trim(' ')));
    }

    public void Load(string param) => dCore.InitDialog(param);

    //public void ReturnToSaved(string param) => dCore.ReturnToSaved(1);
    //public void ReturnToSaved(string param) => dCore.ReturnToSaved(-1);

    public void SetBackground(string param) => backgroundsCtr.SetBack(param);
    public void SetBackgroundLong(string param) => backgroundsCtr.SetLongBack(param);
    public void Focus(string param) => dFocusing.Focus(param);
    public void FocusMove(string param) => dFocusing.FocusMove(param);
    public void UnFocus(string param) => dFocusing.UnFocus(param);
    public void UnFocusMove(string param) => dFocusing.UnFocusMove(param);
    public void FocusBack(string param) => dFocusing.FocusBack();
    public void UnFocusBack(string param) => dFocusing.UnFocusBack();
    //public void SetAudio(string param) => dSoundManager.StartAudio(param);
    public void SetAudio(string param)
    {
        var parameters = param.Split(',');
        if (parameters.Length == 1) dSoundManager[0].StartAudio(param);
        else dSoundManager[Int32.Parse(parameters[1])].StartAudio(parameters[0]);
    }
    public void StopAudio()
    {
        dSoundManager[0].StopAudio();
    }
    public void StopTargetAudio(string param)
    {
        dSoundManager[Int32.Parse(param)].StopAudio();
    }
    public void StopAllAudio()
    {
        foreach (var i in dSoundManager)
            i.StopAudio();
    }
    public void SwapBackground(string param)
    {
        dCore.HideDialodue();
        backGroundAnimation.SwapBack(param);
    }
    public void SwapBackgroundLong(string param)
    {
        dCore.HideDialodue();
        backGroundAnimation.SwapBackLong(param);
    }
    public void SwapBackground2(string param)
    {
        dCore.HideDialodue();
        backGroundAnimation.SwapBack2(param);
    }
    public void SetFiller(string param) => backGroundAnimation.SetFiller(param);
    public void SetExpression(string param)
    {
        var parameters = param.Split(',');
        DModel model = GetModel(parameters[0].Trim(' '));
        if(parameters.Length > 1)
            model.SetExpression((parameters[1].Trim(' ')));
    }
    public void SetAnimatorBool(string param)
    {
        var parameters = param.Split(',');
        DModel model = GetModel(parameters[0].Trim(' '));
        model.SetAnimatorBool((parameters[1].Trim(' ')),Boolean.Parse(parameters[2].Trim(' ')) );
    }
    public void Close() => dCore.Close();
    public void PrintLog(string log) => dCore.PrintLog(log);
    public void AddFlag(string param) => GraphAction.AddFlag(param);
    public void RiseFlag(string param) => GraphAction.RiseFlag(param);
    public void RemoveFlag(string param) => GraphAction.RemoveFlag(param);
    public void LowerFlag(string param) => GraphAction.LowerFlag(param);
    public void AddVarWithCurrentDayNumber(string param) => GraphAction.AddVarWithCurrentDayNumber(param);
    public void AddVarWithCurrentDayNumberPLUS(string param) => GraphAction.AddVarWithCurrentDayNumberPLUS(param);
    public void CreateVar(string param) => GraphAction.CreateVar(param);
    public void AddVar(string name, int rep) => GraphAction.AddVar(name, rep);
    public void AddVar(string param) => GraphAction.AddVar(param);
    public void RemoveVar(string param) => GraphAction.RemoveVar(param);
    public void RemoveVar(string codename, int val) => GraphAction.RemoveVar(codename, val);
    public void DeleteVar(string param) => GraphAction.DeleteVar(param);
    public void SetVar(string param) => GraphAction.SetVar(param);
    // String Var
    public void CreateStringVar(string param) => GraphAction.CreateStringVar(param);
    public void SetStringVar(string key, string value) => GraphAction.SetStringVar(key, value);
    public void SetStringVar(string param) => GraphAction.SetStringVar(param);
    public void DeleteStringVar(string param) => GraphAction.DeleteStringVar(param);
    public void AddExp(int exp) => GraphAction.AddExp(exp);

    
    //////////////////////////////////////////////////////////////////// Conditions
    public bool isFlagExist(string flag) => GraphAction.isFlagExist(flag);
    public bool isFlagNotExist(string flag) => GraphAction.isFlagNotExist(flag);
    public bool isItemExist(string ItemCodeName) => GraphAction.isItemExist(ItemCodeName);
    public bool isFlagRaised(string flag) => GraphAction.isFlagRaised(flag);
    public bool isFlagNotRaised(string flag) => GraphAction.isFlagNotRaised(flag);
    public bool isVarExist(string name) => GraphAction.isVarExist(name);
    public bool isVarEqual(string name, int rep) => GraphAction.isVarEqual(name, rep);
    public bool isVarEqualDayCounter(string name) => GraphAction.isVarEqualDayCounter(name);
    public bool isVarGreater(string name, int rep) => GraphAction.isVarGreater(name, rep);
    public bool isVarGreaterDayCounter(string name) => GraphAction.isVarGreaterDayCounter(name);
    public bool isVarLess(string name, int rep) => GraphAction.isVarLess(name, rep);
    public bool isVarLessDayCounter(string name) => GraphAction.isVarLessDayCounter(name);
    public bool isStringVarExist(string param) => GraphAction.isStringVarExist(param);
    public bool isStringVarEqual(string paramName, string paramVal) => GraphAction.isStringVarEqual(paramName, paramVal);
    public bool isStringVarEqual(string param) => GraphAction.isStringVarEqual(param);
    public bool isTimeBetween(int GreaterOrEqual, int Less) => GraphAction.isTimeBetween(GreaterOrEqual, Less);
    public bool isSceneNameEqualTo(string name) => GraphAction.isSceneNameEqualTo(name);
}
