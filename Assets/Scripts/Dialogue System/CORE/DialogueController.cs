using System;
using System.Collections;
using System.Collections.Generic;
using Dialogue;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class DialogueController : MonoBehaviour
{
    public SceneDialogue Graph;
    [SerializeField] private DialoguePrinting dPrinting;
    [SerializeField] public DialogueCommands dCommands;
    [SerializeField] public DialogueChoices nChoices;
    [SerializeField] private DView dView;
    [SerializeField] private DSpeaker dSpeaker;

    private Profile profile;
    private bool isInputAwaiting = false;
    public bool GetIsInputAwaiting() => isInputAwaiting;

    public void SetIsInputAwaiting(bool b)
    {
        isInputAwaiting = b;
    }

    private bool isAwaiting = false;

    private DialogueGraph dgraph;

    public void updateDGraph()
    {
        dgraph = (DialogueGraph) Graph.graph;
    }

    public DialogueGraph GetGraph() => dgraph;
    private Chat triggerNext = null;

    private void Awake()
    {
        profile = FindObjectOfType<Profile>();
    }
    
    public Profile PROFILE()
    {
        if(profile == null) Awake();
        return profile;
    }

    public void localisationFunc()
    {
        if (Graph == null || dgraph.current == null)
            return;

        dSpeaker.Speak((dgraph.current as Chat).WhoTalk.name);

        if (profile == null) Awake();
        if (profile.language == Language.EN)
            dPrinting.PrintLine((dgraph.current as Chat).textEN);
        else
            dPrinting.PrintLine((dgraph.current as Chat).text);

        if ((dgraph.current as Chat).answers.Count + (dgraph.current as Chat).answersRight.Count > 0)
            nChoices.Localize(profile.language);

        dPrinting.StopCorr();
    }

    [NonSerialized] private bool DoNotClose = false;

    public void DoNotCallCloseFunc()
    {
        DoNotClose = true;
    }

    public void InitDialog(string tname = "")
    {
        DoNotClose = false;
        if (Graph == null) return;
        dPrinting.CloseAllNotDialogueWindows();

        if (dView.ShowDialogue())
        {
            foreach (var of in openFuncs)
            {
                of.Invoke();
            }
        }

        isInputAwaiting = false;
        
        var tmp  = dgraph = (DialogueGraph) Graph.graph;
        dgraph.Restart();
        
        if (tmp == dgraph)
        {
            triggerNext = (dgraph.current as Chat);
            NextLine();
        }
    }

    public void InitSave(int nodeIndex, int dialogue_index)
    {
        DoNotClose = false;
        if (profile == null) Awake();
        if (Graph == null || Graph.graph == null) return;


        dgraph = (DialogueGraph) Graph.graph;

        dgraph.SetCurrentChatIndex(nodeIndex);
        if (dgraph.currNode == null || dgraph.current == null) return;


        if (dgraph.current.GetType() != typeof(Chat) && dgraph.current.GetType() != typeof(DialogueLine))
        {
            dgraph.current.Trigger();
        }

        if (dgraph != null && dgraph.currNode != null && dgraph.currNode.GetType() == typeof(DialogueLine))
            (dgraph.currNode as DialogueLine).SetI(dialogue_index);


        triggerNext = (dgraph.current as Chat);

        if (dView.ShowDialogue())
        {
            foreach (var of in openFuncs)
            {
                of.Invoke();
            }
        }

        isInputAwaiting = false;
        NextLine(true);
    }

    public void NextLine(bool save = false)
    {
        if (isInputAwaiting)
        {
            return;
        }

        if (Graph == null || dgraph == null || dgraph.current == null)
        {
            Close();
            return;
        }

        if (isAwaiting)
        {
            triggerNext = (dgraph.current as Chat);
            return;
        }

        //StopSpeak();
        if (dView.ShowDialogue())
        {
            foreach (var of in openFuncs)
            {
                of.Invoke();
            }
        }

        if (profile == null) Awake();
        if (save == false)
        {
            LogLine _LogLine = new LogLine();
            _LogLine.type = LogType.Chat;
            _LogLine.WhoTalk = profile.gameList.GetCharacter((dgraph.current as Chat).WhoTalk.name);
            _LogLine.textEN = (dgraph.current as Chat).textEN;
            _LogLine.text = (dgraph.current as Chat).text;
            _LogLine.Tags = (dgraph.current as Chat).Tags;
            _LogLine.Expressions = (dgraph.current as Chat).Expressions;
            _LogLine.OnEnd = (dgraph.current as Chat).OnEnd;

            profile.AddLogLine(_LogLine);
        }
        dCommands.GetDebbuger().AddGraph(Graph, profile);

        try
        {
            dSpeaker.Speak((dgraph.current as Chat).WhoTalk.name);
        }
        catch (Exception e)
        {
            dCommands.AddDebuggerError("<color=#0F0>Wrong speaker name </color>'<color=#F00>" + (dgraph.current as Chat).WhoTalk + "</color>'");
            Debug.Log(e);
        }


        if (save == false)
        {
            if ((dgraph.current as Chat).Tags != "" && (dgraph.current as Chat).Tags != " ")
            {
                var tagList = (dgraph.current as Chat).Tags.Split(';');

                foreach (var item in tagList)
                {
                    dCommands.EnterCommad(item);
                }
            }

            if ((dgraph.current as Chat).Expressions != "" && (dgraph.current as Chat).Expressions != " ")
            {
                var expList = (dgraph.current as Chat).Expressions.Split(';');
                foreach (var item in expList)
                {
                    if (item != "")
                    {
                        dCommands.EnterCommad("SetExpression" + item);
                    }
                }
            }
        }

        //dSpeaker.ShowSpeaker();
        if ((dgraph.current as Chat).answers.Count + (dgraph.current as Chat).answersRight.Count > 0)
        {
            isInputAwaiting = true;

            nChoices.InitChoices((dgraph.current as Chat).answers, (dgraph.current as Chat).answersRight,
                profile.language);
        }

        if (save == false)
        {
            StartCoroutine(printCorr());
        }
        else
        {
            var str = profile.language == Language.RU ? (dgraph.current as Chat).text : (dgraph.current as Chat).textEN;

            str = DialoguePrinting.ReplaceColor(str);
            str = DialoguePrinting.RemoveComands(str);
            str = DLineFormatter.Format(str);

            dPrinting.printingField.text = str;
        }

        triggerNext = null;
    }

    public IEnumerator printCorr()
    {
        if (profile == null) Awake();
        switch (profile.language)
        {
            case Language.EN:
                yield return dPrinting.PrintLineCor((dgraph.current as Chat).textEN);
                break;
            case Language.RU:
                yield return dPrinting.PrintLineCor((dgraph.current as Chat).text);
                break;
        }
    }

    void StopSpeak()
    {
        foreach (var VARIABLE in dSpeaker.GetCharacters().GetModelsInSceneDic())
        {
            if (VARIABLE.Value != null && VARIABLE.Value.gameObject.activeSelf &&
                VARIABLE.Value.GetModel().activeSelf)
                dCommands.EnterCommad("SetExpression(" + VARIABLE.Key +
                                      "," + VARIABLE.Value.GetCurrentExpression()
                                          .Replace("_Talk", "")
                                          .Replace(".anim", "")
                                      + ")");
        }
    }

    public void NextAnswer(int i)
    {
        if (!dgraph.IsAnswerOutputExist(i))
        {
            Close();
            return;
        }

        var j = i;
        if (profile == null) Awake();
        LogLine _LogLine = new LogLine();
        _LogLine.type = LogType.Choosen;
        _LogLine.WhoTalk = profile.gameList.GetCharacter("Player");

        foreach (var VARIABLE in (dgraph.current as Chat).answers)
        {
            if (VARIABLE.ConditionsInvoke())
            {
                _LogLine.text += VARIABLE.text + ";";
                _LogLine.textEN += VARIABLE.textEN + ";";
            }
            else
            {
                i--;
            }
        }

        foreach (var VARIABLE in (dgraph.current as Chat).answersRight)
        {
            if (VARIABLE.ConditionsInvoke())
            {
                _LogLine.text += VARIABLE.text + ";";
                _LogLine.textEN += VARIABLE.textEN + ";";
            }
            else
            {
                i--;
            }
        }

        _LogLine.text = i + ";" + _LogLine.text;
        _LogLine.textEN = i + ";" + _LogLine.textEN;

        _LogLine.Tags = (dgraph.current as Chat).Tags;
        _LogLine.Expressions = (dgraph.current as Chat).Expressions;
        _LogLine.OnEnd = (dgraph.current as Chat).OnEnd;


        profile.LogPhrases.Add(_LogLine);

        dgraph.AnswerQuestion(j);

        if (dgraph.current == null)
        {
            Close();
            return;
        }

        isInputAwaiting = false;
        NextLine();
    }

    public void StopInput()
    {
        isAwaiting = true;
    }

    public void ContinuetInput()
    {
        isAwaiting = false;
        NextLine();
    }

    public void ContinuetDialodueInput()
    {
        isAwaiting = false;
        NextLine();
    }

    public void ContinuetDialog()
    {
        isInputAwaiting = false;
        NextLine();
    }

    public void InvokeOnEnd()
    {
        if (dgraph == null || dgraph.current == null)
        {
            Close();
            return;
        }

        if ((dgraph.current as Chat).answers.Count + (dgraph.current as Chat).answersRight.Count == 0)
        {
            dPrinting.TextEmpty();
            //dSpeaker.HideSpeaker();
        }

        if ((dgraph.current as Chat).OnEnd != "" && (dgraph.current as Chat).OnEnd != " ")
        {
            var tagList = (dgraph.current as Chat).OnEnd.Split(';');
            foreach (var item in tagList)
                dCommands.EnterCommad(item);
        }

        if (triggerNext == null)
        {
            if ((dgraph.current as Chat).answers.Count + (dgraph.current as Chat).answersRight.Count == 0)
            {
                (dgraph.current as Chat).AnswerQuestion(0);
                if (dgraph == null || dgraph.current == null) Close();
            }
        }
        else
        {
            dgraph.current = triggerNext;
            //triggerNext = null;
        }
    }

    public List<UnityEvent> openFuncs;
    public List<UnityEvent> closeFuncs;


    public void PrintLog(string log)
    {
        Debug.Log(log);
    }
    public void Close()
    {
        Graph = null;
        dgraph = null;
        dSpeaker.HideSpeakerEnd();
        StopSpeak();
        dView.HideDialog();


        if (DoNotClose)
        {
            DoNotClose = false;
            return;
        }

        foreach (var cf in closeFuncs)
        {
            cf.Invoke();
        }
    }

    public void HideDialodue()
    {
        dView.HideDialog();
    }

    public void ShowDialodue()
    {
        dView.InitDialog();
    }

    public void SetAndStartGraph(SceneDialogue sg)
    {
        nChoices.HideChoices();
        SetGraph(sg);
        InitDialog();
    }
    public bool isFlagExist(string flag)
    {
        if (profile == null) Awake();
        return profile.isFlagExist(flag);
    }
    public bool isFlagNotExist(string flag)
    {
        if (profile == null) Awake();
        return !profile.isFlagExist(flag);
    }
    public bool isItemExist(string ItemCodeName)
    {
        if (profile == null) Awake();
        return profile.isItemExist(ItemCodeName);
    }
    public bool isFlagRaised(string flag)
    {
        if (profile == null) Awake();
        return isFlagExist(flag) && profile.IsFlagRaised(flag);
    }
    public bool isFlagNotRaised(string flag)
    {
        if (profile == null) Awake();
        return isFlagExist(flag) && !profile.IsFlagRaised(flag);
    }
    public bool isVarExist(string name)
    {
        if (profile == null) Awake();
        return profile.isVarExist(name);
    }
    public bool isVarEqual(string name, int rep)
    {
        if (profile == null) Awake();
        return profile.isVarEqual(name, rep);
    }
    public bool isVarEqualDayCounter(string name)
    {
        if (profile == null) Awake();
        return profile.isVarEqual(name, profile.DayCounter);
    }
    public bool isVarGreater(string name, int rep)
    {
        if (profile == null) Awake();
        return profile.isVarGreater(name, rep);
    }
    public bool isVarGreaterDayCounter(string name)
    {
        if (profile == null) Awake();
        return profile.isVarGreater(name, profile.DayCounter);
    }
    public bool isVarLess(string name, int rep)
    {
        if (profile == null) Awake();
        return profile.isVarLess(name, rep);
    }
    public bool isVarLessDayCounter(string name)
    {
        if (profile == null) Awake();
        return profile.isVarLess(name, profile.DayCounter);
    }
    public bool isTimeBetween(int GreaterOrEqual, int Less)
    { 
        if (profile == null) Awake();
        int hour = Int32.Parse(profile._time.Split(':')[0]);
        return GreaterOrEqual <= hour && hour < Less;
    }
    public bool isSceneNameEqualTo(string name)
    {
        return SceneManager.GetActiveScene().name.Equals(name);
    }

    public void SetGraph(SceneDialogue sd)
    {
        if (sd == null) return;
        Graph = sd;
    }

}