
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BackGroundAnimation : MonoBehaviour
{
    [SerializeField] private Image fillerImg;
    [SerializeField] private Backgrounds backgrounds;
    [SerializeField] private DialogueInput input;
    [SerializeField] private CanvasGroup black, all,filer;
    [SerializeField] private DialogueCommands dActions;
    [SerializeField] private DialogueController dCore;
    [SerializeField] private SwapInput swapInput;
    [SerializeField] private SwapAnimation swapAnim;
    //[SerializeField] private Backgrounds backgrounds;
    public float animTime = .7f;

    public void SwapBack(string backName)
    {
        var param = backName.Split(',');
        input.StopInputWaiting();
        dCore.StopInput();

        if (param.Length == 1)
        {
            StartCoroutine(SwapInCor(param[0], false));
            
        }
        else
        {
            if(param[0] == "")
                StartCoroutine(SwapInCor(param[1], false));
            else
                StartCoroutine(SwapInCor(param[0], param[1]));
            
        }
    }

    public void SetFiller(string name)
    {
        fillerImg.sprite = backgrounds.GetSprite(name);
    }
    
    public void SwapBackLong(string backName)
    {
        
        var param = backName.Split(',');
        input.StopInputWaiting();
        dCore.StopInput();

        if(param[0] == "")
            StartCoroutine(SwapInCor(param[1], true));
        else
            StartCoroutine(SwapInCor(param[0], param[1], true));
    }
    public void SwapBack2(string backName)
    {
        var param = backName.Split(',');
        input.StopInputWaiting();
        dCore.StopInput();

        StartCoroutine(SwapInCor2(param[0], param[1]));
    }
    IEnumerator SwapInCor2(string backName, string newBack)
    {
        swapAnim.Deactive();

        LeanTween.alphaCanvas(black, 1, animTime);
        yield return new WaitForSeconds(animTime);
        
        filer.alpha = 1;
        fillerImg.sprite = backgrounds.GetSprite(backName);
        LeanTween.alphaCanvas(black, 0, animTime / 2);
        yield return new WaitForSeconds(animTime);

        LeanTween.alphaCanvas(black, 1, animTime);
        yield return new WaitForSeconds(animTime);
        
        fillerImg.sprite = backgrounds.GetSprite(newBack);
        LeanTween.alphaCanvas(black, 0, animTime / 2);
        yield return new WaitForSeconds(animTime/2);
        
        filer.blocksRaycasts = true;
        filer.interactable = true;
        swapInput.StartInputWaiting();
        swapAnim.Active();
    }

    public void ContinueSwap()
    {
        StartCoroutine(SwapOutCor());
    }

    
    
    IEnumerator SwapInCor(string backName, string newBack, bool isLong = false)
    {
        swapAnim.Deactive();
        LeanTween.alphaCanvas(black, 1, animTime);
        yield return new WaitForSeconds(animTime);
        
        filer.alpha = 1;
        
        if(isLong) dActions.SetBackgroundLong(newBack);
        else dActions.SetBackground(newBack);
        
        LeanTween.alphaCanvas(black, 0, animTime / 2);
       // var split = backName.Split('|');
        
        if (swapAnim.IsAnimation(backName))
        {
            //fillerImg.sprite = backgrounds.GetSprite(split[1]);
            yield return StartCoroutine(swapAnim.TryToPlayAnim(backName));
        }
        else
        {
            fillerImg.sprite = backgrounds.GetSprite(backName);
            yield return new WaitForSeconds(animTime / 2);
        }
        filer.blocksRaycasts = true;
        filer.interactable = true;
        swapInput.StartInputWaiting();
        swapAnim.Active();
    }

    IEnumerator SwapInCor(string newBack, bool isLong = false)
    {
        swapAnim.Deactive();
        LeanTween.alphaCanvas(black, 1, animTime);
        yield return new WaitForSeconds(animTime);
        //filer.alpha = 1;
        
        if(isLong) dActions.SetBackgroundLong(newBack);
        else dActions.SetBackground(newBack);
        
        LeanTween.alphaCanvas(black, 0, animTime / 2);
        yield return new WaitForSeconds(animTime / 2);
        //s//wapInput.StartInputWaiting();
        
        input.StartInputWaiting();
        dCore.ContinuetInput();
        swapAnim.Active();
    }

    IEnumerator SwapOutCor()
    {
        swapAnim.Deactive();
        filer.blocksRaycasts = false;
        filer.interactable = false;
        LeanTween.alphaCanvas(black, 1, animTime);
        yield return new WaitForSeconds(animTime);
        filer.alpha = 0;
        LeanTween.alphaCanvas(black, 0, animTime / 2);
        yield return new WaitForSeconds(animTime / 2);
        
        input.StartInputWaiting();
        dCore.ContinuetInput();
        swapAnim.Active();
    }

    public void JustLoadWithBlack(string scene)
    {
        StartCoroutine(TO_BLACK_AND_LOAD(scene));
    }

    public void LoadWithBlack(string scene)
    {
        dActions.StopAudio();
        input.StopInputWaiting();
        dCore.StopInput();
        swapAnim.Deactive();
        
        StartCoroutine(TO_BLACK_AND_LOAD(scene));
    }

    IEnumerator TO_BLACK_AND_LOAD(string scene)
    {
        LeanTween.alphaCanvas(black, 1, animTime);
        yield return new WaitForSeconds(animTime);

        SceneManager.LoadScene(scene);
    }

    private Coroutine _coroutine = null;
    public void GoFromBlack()
    {
        if(_coroutine != null) StopCoroutine(_coroutine);
        input.StopInputWaiting();
        swapAnim.Deactive();
        black.alpha = 1;
          _coroutine =  StartCoroutine(FROM_BLACK());
    }
    IEnumerator FROM_BLACK()
    {
        LeanTween.alphaCanvas(black, 0, animTime);
        yield return new WaitForSeconds(animTime);

        input.StartInputWaiting();
        swapAnim.Active();
    }
    public void GoToBlack()
    {
        
        if(_coroutine != null) StopCoroutine(_coroutine);
        
        input.StopInputWaiting();
        swapAnim.Deactive();
        black.alpha = 0;
        _coroutine = StartCoroutine(TO_BLACK());
    }
    IEnumerator TO_BLACK()
    {
        LeanTween.alphaCanvas(black, 1, animTime);
        yield return new WaitForSeconds(animTime);

        input.StartInputWaiting();
        swapAnim.Active();
    }
    public void GoToAndFromBlack()
    {
        
        if(_coroutine != null) StopCoroutine(_coroutine);
        
        input.StopInputWaiting();
        swapAnim.Deactive();
        black.alpha = 0;
        _coroutine = StartCoroutine(TO_BLACK_AND_FROM());
    }
    IEnumerator TO_BLACK_AND_FROM()
    {
        LeanTween.alphaCanvas(black, 1, animTime);
        yield return new WaitForSeconds(animTime);

        LeanTween.alphaCanvas(black, 0, animTime);
        
        input.StartInputWaiting();
        swapAnim.Active();
    }

    public void SetAnimTime(float time)
    {
        animTime = time;
    }

}

