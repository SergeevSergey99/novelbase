using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GraphAction : MonoBehaviour
{
    public static void SetProfileTime(string time) => PROFILE()._time = time;

    public static void SetTime(string time)
    {
        GameObject g = GameObject.Find("INFOS");
        GameObject gg = null;
        if (g != null) gg = g.transform.Find("Time").gameObject;
        if (gg != null) gg.GetComponent<TMP_Text>().text = time;
        if (g != null) gg = g.transform.Find("TimeImage").gameObject;
        //if (gg != null) gg.GetComponent<Image>().sprite = PROFILE().gameList.GetTimeIcon(time);

        var t = time.Split(':');
        int hour = Int32.Parse(t[0]);
        int minutes = Int32.Parse(t.Length > 1 ? t[1] : "00");
        PROFILE()._time = hour.ToString() + ":" + (minutes < 10 ? "0" : "") + minutes.ToString();
    }

    public static void SetProfileDay(string day) => PROFILE()._day = Int32.Parse(day);

    public static void AddProfileDay(string day)
    {
        var prf = PROFILE();
        prf._day += Int32.Parse(day);

        for (int i = prf.FlagVariables.Count; i >= 0; i++)
        {
            if(prf.FlagVariables[i].name.Contains("_DayDelete")) prf.FlagVariables.RemoveAt(i);
            else if(prf.FlagVariables[i].name.Contains("_DayLower")) prf.FlagVariables[i].isRaised = false;
            else if(prf.FlagVariables[i].name.Contains("_DayRise")) prf.FlagVariables[i].isRaised = true;
        }
        
        if (prf._day > 7)
        {
            for (int i = prf.FlagVariables.Count; i >= 0; i++)
            {
                if(prf.FlagVariables[i].name.Contains("_WeekDelete")) prf.FlagVariables.RemoveAt(i);
                else if(prf.FlagVariables[i].name.Contains("_WeekLower")) prf.FlagVariables[i].isRaised = false;
                else if(prf.FlagVariables[i].name.Contains("_WeekRise")) prf.FlagVariables[i].isRaised = true;
            }
            
            prf._day = (prf._day - 1) % 7 + 1;
        }

        prf.DayCounter += Int32.Parse(day);
    }

    public static void SetDay(string day)
    {
        Profile prf = PROFILE();
        GameObject g = GameObject.Find("INFOS");
        if (g == null) return;
        GameObject gg = null;

        LocalizedString str = PROFILE().gameList.days[Int32.Parse(day) - 1];
        if (g != null) gg = g.transform.Find("Day").gameObject;
        if (gg != null)
        {
            gg.GetComponent<TMP_Text>().text = (prf.language == Language.EN) ? str.EN : str.RU;
            gg.GetComponent<LocalizationScript>().EN_Str = str.EN;
            gg.GetComponent<LocalizationScript>().RU_Str = str.RU;
        }

        prf._day = Int32.Parse(day);
    }

    public static void AddItem(string codeName)
    {
        var list = PROFILE().gameList.inventoryObjects;
        var index = list.FindIndex(c => c.name.Equals(codeName));

        if (index >= 0)
        {
            PROFILE().AddItem(list[index]);
        }
        else
            FindObjectOfType<DialogueCommands>()
                .AddDebuggerError("<color=#0F0>Wrong itemName </color>'<color=#F00>" + codeName + "</color>'");
    }

    public static void RemoveItem(string codeName)
    {
        PROFILE().RemoveItem(codeName);
    }

    public static void ReplaceItem(string codeNames)
    {
        var prf = PROFILE();
        var names = codeNames.Split(',');
        var list = prf.gameList.inventoryObjects;
        var index2 = list.FindIndex(c => c.name.Equals(names[1]));
        if (index2 >= 0)
        {
            var codeName = names[0];
            if (prf.isItemExist(codeName)) prf.ReplaceItem(codeName, list[index2]);
            else
                FindObjectOfType<DialogueCommands>()
                    .AddDebuggerError("<color=#0F0>No item in Profile </color>'<color=#F00>" + codeName + "</color>'");
        }
        else
            FindObjectOfType<DialogueCommands>()
                .AddDebuggerError("<color=#0F0>Wrong itemName </color>'<color=#F00>" + names[1] + "</color>'");
    }

    public static void AddFlag(string param) => PROFILE().AddFlag(param);
    public static void RemoveFlag(string param) => PROFILE().RemoveFlag(param);
    public static void RiseFlag(string param) => PROFILE().RiseFlag(param);
    public static void LowerFlag(string param) => PROFILE().LowerFlag(param);
    public static void CreateVar(string param) => PROFILE().CreateVar(param);

    public static void AddVar(string param)
    {
        var parameters = param.Split(',');
        PROFILE().AddVar(parameters[0], Int32.Parse(parameters[1]));
    }

    public static void RemoveVar(string param)
    {
        var parameters = param.Split(',');
        PROFILE().RemoveVar(parameters[0], parameters.Length == 1 ? 1 : Int32.Parse(parameters[1]));
    }

    public static void CheckLeveling()
    {
        var prf = PROFILE();
        if (!prf.isVarExist("Exp"))
        {
            prf.CreateVar("Exp");
            prf.CreateVar("Level");
            prf.CreateVar("UpgradePoints");
        }
    }

    public static void AddExp(int exp)
    {
        int[] maxExpForLevel = {15, 20, 30, 40, 50, 60, 75, 105, 125, 150, 175, 200, 225, 250};
        var prf = PROFILE();

        CheckLeveling();

        if (prf.isVarLess("Level", 15))
        {
            int _level = prf.GetVar("Level");
            int _exp = prf.GetVar("Exp");

            _exp += exp;
            if (_exp >= maxExpForLevel[_level])
            {
                _exp -= maxExpForLevel[_level];
                prf.AddVar("Level", 1);
                prf.AddVar("UpgradePoints", 1);
            }

            prf.SetVar("Exp", _exp);
        }

        if (!prf.isVarLess("Level", 15))
            prf.SetVar("Exp", 0);
    }
    public static void AddVarWithCurrentDayNumber(string param) => AddVar(param + ',' + PROFILE().DayCounter);

    public static void AddVarWithCurrentDayNumberPLUS(string param)
    {
        var parameters = param.Split(',');
        AddVar(parameters[0] + ',' + (PROFILE().DayCounter + Int32.Parse(parameters[1])));
    }

    public static void DeleteVar(string param) => PROFILE().DeleteVar(param);

    public static void SetVar(string param)
    {
        var parameters = param.Split(',');
        PROFILE().SetVar(parameters[0], Int32.Parse(parameters[1]));
    }


    public static void RemoveVar(string name, int rep) => PROFILE().RemoveVar(name, rep);
    public static void AddVar(string name, int rep) => PROFILE().AddVar(name, rep);

    public static void CreateStringVar(string param)
    {
        var parameters = param.Split(',');
        PROFILE().CreateStringVar(parameters[0], parameters[1]);
    }

    public static void SetStringVar(string key, string value) => PROFILE().SetStringVar(key, value);

    public static void SetStringVar(string param)
    {
        var parameters = param.Split(',');
        SetStringVar(parameters[0], parameters[1]);
    }

    public static void DeleteStringVar(string param)
    {
        var parameters = param.Split(',');
        PROFILE().DeleteStringVar(parameters[0]);
    }

    ////////////////////////////////////////////////////////////////////
    public static bool isFlagExist(string flag) => PROFILE().isFlagExist(flag);
    public static bool isFlagNotExist(string flag) => !PROFILE().isFlagExist(flag);
    public static bool isItemExist(string ItemCodeName) => PROFILE().isItemExist(ItemCodeName);
    public static bool isFlagRaised(string flag) => isFlagExist(flag) && PROFILE().IsFlagRaised(flag);
    public static bool isFlagNotRaised(string flag) => isFlagExist(flag) && !PROFILE().IsFlagRaised(flag);

    public static bool isVarExist(string name) => PROFILE().isVarExist(name);

    public static bool isVarEqual(string name, int rep) => PROFILE().isVarEqual(name, rep);
    public static bool isVarEqualDayCounter(string name) => PROFILE().isVarEqual(name, PROFILE().DayCounter);

    public static bool isVarGreater(string name, int rep) => PROFILE().isVarGreater(name, rep);

    public static bool isVarGreaterDayCounter(string name) => PROFILE().isVarGreater(name, PROFILE().DayCounter);
    public static bool isVarLess(string name, int rep) => PROFILE().isVarLess(name, rep);
    public static bool isVarLessDayCounter(string name) => PROFILE().isVarLess(name, PROFILE().DayCounter);
    public static bool isStringVarExist(string param) => PROFILE().isStringExist(param);

    public static bool isStringVarEqual(string paramName, string paramVal) => PROFILE().isStringExist(paramName) &&
                                                                              PROFILE().GetStringVar(paramName)
                                                                                  .Equals(paramVal);

    public static bool isStringVarEqual(string param)
    {
        var parameters = param.Split(',');
        return isStringVarEqual(parameters[0], parameters[1]);
    }

    public static bool isTimeBetween(int GreaterOrEqual, int Less) =>
        GreaterOrEqual <= Int32.Parse(PROFILE()._time.Split(':')[0]) &&
        Int32.Parse(PROFILE()._time.Split(':')[0]) < Less;

    public static bool isSceneNameEqualTo(string name) => SceneManager.GetActiveScene().name.Equals(name);

    private static Profile _profile;

    static Profile PROFILE()
    {
        if (_profile == null) _profile = FindObjectOfType<Profile>();
        return _profile;
    }

    public static void PrintLog()
    {
        Debug.Log("Log");
    }

    // NodeActions funcs
    public void Enter(string param) => FindObjectOfType<DialogueCommands>().Enter(param);
    public void Exit(string param) => FindObjectOfType<DialogueCommands>().Exit(param);
    public void RemoveParameter(string param) => FindObjectOfType<DialogueCommands>().RemoveParameter(param);
    public void RemoveParameterAtIndex(string param) => FindObjectOfType<DialogueCommands>().RemoveParameterAtIndex(param);
    public void SetParamCONST(string param) => FindObjectOfType<DialogueCommands>().SetParamCONST(param);
    public void SetParamLERP(string param) => FindObjectOfType<DialogueCommands>().SetParamLERP(param);
    public void SetParamSIN(string param) => FindObjectOfType<DialogueCommands>().SetParamSIN(param);
    public void SetParamCOS(string param) => FindObjectOfType<DialogueCommands>().SetParamCOS(param);
    public void SetModelColor(string param) => FindObjectOfType<DialogueCommands>().SetModelColor(param);
    public void SetModelColorHex(string param) => FindObjectOfType<DialogueCommands>().SetModelColorHex(param);
    public void MoveTo(string param) => FindObjectOfType<DialogueCommands>().MoveTo(param);
    public void MoveAltTo(string param) => FindObjectOfType<DialogueCommands>().MoveAltTo(param);
    public void SetPosTo(string param) => FindObjectOfType<DialogueCommands>().SetPosTo(param);
    public void SetAltPosTo(string param) => FindObjectOfType<DialogueCommands>().SetAltPosTo(param);
    public void MirrorTo(string param) => FindObjectOfType<DialogueCommands>().MirrorTo(param);
    public void SetBackground(string param) => FindObjectOfType<DialogueCommands>().SetBackground(param);
    public void SetBackgroundLong(string param) => FindObjectOfType<DialogueCommands>().SetBackgroundLong(param);
    public void Focus(string param) => FindObjectOfType<DialogueCommands>().Focus(param);
    public void FocusMove(string param) => FindObjectOfType<DialogueCommands>().FocusMove(param);
    public void UnFocus(string param) => FindObjectOfType<DialogueCommands>().UnFocus(param);
    public void UnFocusMove(string param) => FindObjectOfType<DialogueCommands>().UnFocusMove(param);
    public void FocusBack(string param) => FindObjectOfType<DialogueCommands>().FocusBack(param);
    public void UnFocusBack(string param) => FindObjectOfType<DialogueCommands>().UnFocusBack(param);
    public void SetAudio(string param) => FindObjectOfType<DialogueCommands>().SetAudio(param);
    public void StopAudio() => FindObjectOfType<DialogueCommands>().StopAudio();
    public void StopTargetAudio(string param) => FindObjectOfType<DialogueCommands>().StopTargetAudio(param);
    public void StopAllAudio() => FindObjectOfType<DialogueCommands>().StopAllAudio();
    public void SwapBackground(string param) => FindObjectOfType<DialogueCommands>().SwapBackground(param);
    public void SwapBackgroundLong(string param) => FindObjectOfType<DialogueCommands>().SwapBackgroundLong(param);
    public void SwapBackground2(string param) => FindObjectOfType<DialogueCommands>().SwapBackground2(param);
    public void SetFiller(string param) => FindObjectOfType<DialogueCommands>().SetFiller(param);
    public void SetExpression(string param) => FindObjectOfType<DialogueCommands>().SetExpression(param);
    public void SetAnimatorBool(string param) => FindObjectOfType<DialogueCommands>().SetAnimatorBool(param);
    public void PrintLog(string log) => FindObjectOfType<DialogueCommands>().PrintLog(log);
}