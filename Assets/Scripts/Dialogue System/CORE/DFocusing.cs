using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DFocusing : MonoBehaviour
{
    [SerializeField] private DCharacters dCharacters;
    [SerializeField] private DPositioning dPositioning;
    [SerializeField] private float animTime = .1f;
    [SerializeField] private GameObject background;
    private DModel lastFocused;
    private float focusZ = -3;

    public void SpeakFocus(DModel model)
    {
        if (model == lastFocused) return;
        SpeakUnfocus();
        model.ScaleSmall();
        model.MoveToZ(focusZ);
        //if(lastFocused != null)
            //lastFocused.SetExpression(lastFocused.GetCurrentExpression().Replace("_Talk",""));
        lastFocused = model;
        //model.SetExpression(model.GetCurrentExpression().Replace("_Talk","") + "_Talk");
    }

    public void SpeakUnfocus()
    {
        if (lastFocused == null) return;
        lastFocused.ScaleNormal();
        //lastFocused.SetExpression(lastFocused.GetCurrentExpression().Replace("_Talk",""));
        lastFocused.ReturnToDefaultZ();
        lastFocused = null;
    }

    //public void Focus(DModel model)
    public void Focus(string model)
    {
        dCharacters.GetModel(model).ScaleBig();
        dCharacters.GetModel(model).SetTo(dPositioning.GetZoomPos());
        
    }
    public void FocusMove(string model)
    {
        dCharacters.GetModel(model).FocusScaleBig();
        dCharacters.GetModel(model).MoveTo(dPositioning.GetZoomPos());
        
    }

    public void UnFocus(string model)
    {
        //Debug.Log("unfocusing" + model);
        dCharacters.GetModel(model).Unfocus();
    }
    public void UnFocusMove(string model)
    {
        //Debug.Log("unfocusing" + model);
        dCharacters.GetModel(model).UnfocusMove();
    }

    public void FocusBack()
    {
        LeanTween.scale(background, Vector3.one * 1.1f, animTime);
    }

    public void UnFocusBack()
    {
        LeanTween.scale(background, Vector3.one, animTime);
    }
}
