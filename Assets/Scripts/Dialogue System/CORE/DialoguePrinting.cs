
using System;
using System.Collections;
using System.Collections.Generic;
using Dialogue;
using UnityEngine;
using TMPro;

public class DialoguePrinting : MonoBehaviour
{
    //[SerializeField] DLineFormatter format;
    [SerializeField] public DialogueInput input;
    [SerializeField] public TextMeshProUGUI printingField;
    [SerializeField] private float typeTime = .01f;
    private string line;
    private Coroutine printCor = null;

    public bool IsPrinting()
    {
        return printCor != null;
    }

    public struct TextCommand
    {
        public int index;
        public string command;

    }

    private List<TextCommand> textCommands = new List<TextCommand>();
    void Replace(string line)
    {
        textCommands.Clear();
        this.line = DLineFormatter.Format(line);
        this.line = ReplaceColor(this.line);
        this.line = this.line.Replace("<ec=#","<EnterCommand=#");
        this.line = this.line.Replace("<ec#","<EnterCommand=#");
        this.line = this.line.Replace("<EC=#","<EnterCommand=#");
        this.line = this.line.Replace("<EC#","<EnterCommand=#");
        this.line = this.line.Replace("<TAGS=#","<EnterCommand=#");
        this.line = this.line.Replace("<TAGS#","<EnterCommand=#");
        this.line = this.line.Replace("<tags=#","<EnterCommand=#");
        this.line = this.line.Replace("<tags#","<EnterCommand=#");
        this.line = this.line.Replace("<Exp","<EnterCommand=#SetExpression");
        this.line = this.line.Replace("<exp","<EnterCommand=#SetExpression");
        this.line = this.line.Replace("<Exp#","<EnterCommand=#SetExpression");
        this.line = this.line.Replace("<exp#","<EnterCommand=#SetExpression");
        this.line = this.line.Replace("<Exp=#","<EnterCommand=#SetExpression");
        this.line = this.line.Replace("<exp=#","<EnterCommand=#SetExpression");
        
        var tempLine = this.line;//.Replace("<EnterCommand=#", "Æ");

        int index = tempLine.IndexOf("<EnterCommand=#");

        while (index != -1)
        {
            TextCommand tc = new TextCommand();
            tc.index = index + "<EnterCommand=#".Length;
            int end = tempLine.IndexOf(">", index);
            tc.command = tempLine.Substring(tc.index, end - tc.index);
            Debug.Log(tc.command);
            tempLine = tempLine.Substring(0, index) + tempLine.Substring(end+1);
            textCommands.Add(tc);
            tempLine = tempLine.Replace("  ", " ");
            index = tempLine.IndexOf("<EnterCommand=#");
        }

        this.line = tempLine;
    }

    public static string ReplaceColor(string line)
    {

        line = line.Replace("<c=#","<color=#");
        line = line.Replace("<C=#","<color=#");
        line = line.Replace("<c#","<color=#");
        line = line.Replace("<C#","<color=#");
        line = line.Replace("</C>","</color>");
        line = line.Replace("</c>","</color>");
        return line;
    }

    public static string RemoveComands(string line)
    {

        line = line.Replace("<ec=#","<EnterCommand=#");
        line = line.Replace("<ec#","<EnterCommand=#");
        line = line.Replace("<EC=#","<EnterCommand=#");
        line = line.Replace("<EC#","<EnterCommand=#");
        line = line.Replace("<TAGS=#","<EnterCommand=#");
        line = line.Replace("<TAGS#","<EnterCommand=#");
        line = line.Replace("<tags=#","<EnterCommand=#");
        line = line.Replace("<tags#","<EnterCommand=#");
        line = line.Replace("<Exp","<EnterCommand=#SetExpression");
        line = line.Replace("<exp","<EnterCommand=#SetExpression");
        line = line.Replace("<Exp#","<EnterCommand=#SetExpression");
        line = line.Replace("<exp#","<EnterCommand=#SetExpression");
        line = line.Replace("<Exp=#","<EnterCommand=#SetExpression");
        line = line.Replace("<exp=#","<EnterCommand=#SetExpression");
        //line = line.Replace("<EnterCommand=#", "￼");
        var tokens = line.Split("<EnterCommand=#");
        var str = tokens[0];
        for(var i = 1;i<tokens.Length;i++)
        {
            var tmp = tokens[i].Split('>');
            if (tmp.Length > 1)
                str += tmp[1];
        }

        line = str;
        return line;
    }

    public void DoCommands()
    {
        foreach (var tc in textCommands)
        {
            
            var tagList = tc.command.Split(';');
            foreach (var item in tagList)
            {
                if(input != null) input.nCore.dCommands.EnterCommad(item);
            }
            Debug.Log("Выполнены: " + tc.command);
        }
    }
    public void DoCommands(string line)
    {

        line = line.Replace("<ec=#","<EnterCommand=#");
        line = line.Replace("<ec#","<EnterCommand=#");
        line = line.Replace("<EC=#","<EnterCommand=#");
        line = line.Replace("<EC#","<EnterCommand=#");
        line = line.Replace("<TAGS=#","<EnterCommand=#");
        line = line.Replace("<TAGS#","<EnterCommand=#");
        line = line.Replace("<tags=#","<EnterCommand=#");
        line = line.Replace("<tags#","<EnterCommand=#");
        line = line.Replace("<Exp","<EnterCommand=#SetExpression");
        line = line.Replace("<exp","<EnterCommand=#SetExpression");
        line = line.Replace("<Exp#","<EnterCommand=#SetExpression");
        line = line.Replace("<exp#","<EnterCommand=#SetExpression");
        line = line.Replace("<Exp=#","<EnterCommand=#SetExpression");
        line = line.Replace("<exp=#","<EnterCommand=#SetExpression");
        
        //line = line.Replace("<EnterCommand=#", "￼");
        var tokens = line.Split("<EnterCommand=#");
        for(var i = 1;i < tokens.Length;i++)
        {
            var tmp = tokens[i].Split('>');
            if (tmp.Length > 1)
                if(input != null) input.nCore.dCommands.EnterCommad(tmp[0]);;
        }

    }
    public void PrintLine(string line)
    {
        Replace(line);

        printingField.text = "";
        GetComponent<SoundManager>().StartAudio(0);
        printCor = StartCoroutine(PrintCor());
        if(input != null) input.StartInputWaiting();
    }

    
    public void CloseAllNotDialogueWindows() => input.CloseAllNotDialogueWindows();
    public bool StopCor()
    {
        if (printCor == null) return false;
        StopCoroutine(printCor);
        printCor = null;
        //DoCommands(line.Substring(i));
        DoCommands();
        printingField.text = RemoveComands(line);
        GetComponent<SoundManager>().Stop();
        return true;
    }
    public void StopCorr()
    {
        if (printCor == null) return;
        StopCoroutine(printCor);
        printCor = null;
        GetComponent<SoundManager>().Stop();
        //DoCommands(line.Substring(i));
        DoCommands();

        textCommands.Clear();
        printingField.text = RemoveComands(line);
    }

    private int i = -1;
    IEnumerator PrintCor()
    {

        WaitForSeconds typeWait = new WaitForSeconds(typeTime);
        for (i = 0; i < line.Length; i++)
        {
            yield return typeWait;
            try
            {

                var r = (int)printingField.color.r;
                var g = (int)printingField.color.g;
                var b = (int)printingField.color.b;
                var hexColor = r.ToString("X2") + g.ToString("X2") + b.ToString("X2");
                printingField.text = "<color=#"+hexColor+">";

                printingField.text += line.Substring(0, i);
                var tmpstr = line.Substring(i);

                if (tmpstr.StartsWith("<"))
                {
                    if (tmpstr.StartsWith("<color=#"))
                    {
                        i += tmpstr.Split('>')[0].Length;
                    }
                    else if(tmpstr.StartsWith("</color>"))
                    {
                        i += "</color>".Length;
                    }
                    else if(tmpstr.StartsWith("<EnterCommand=#"))
                    {
                        var command = tmpstr.Split('>')[0].Split('#')[1];
                        var tagList = command.Split(';');

                        foreach (var item in tagList)
                        {
                            if(input != null) input.nCore.dCommands.EnterCommad(item);
                        }
                        line = line.Substring(0, i) + line.Substring(i + ("<EnterCommand=#" + command + ">").Length);
                        line.Replace("  ", " ");
                    }


                }

                while (textCommands.Count > 0 && i >= textCommands[0].index)
                {
                    var tagList = textCommands[0].command.Split(';');
                    textCommands.RemoveAt(0);
                    foreach (var item in tagList)
                    {
                        if(input != null) input.nCore.dCommands.EnterCommad(item);
                        Debug.Log("Выполнено: " + item);
                    }
                }
                
                printingField.text += "<color=#0000>" + line.Substring(i).Replace("</color>","").Replace("<color=#","");
            }
            catch (Exception e)
            {
                Debug.Log("Exception in graph " + CommandDebugger.GetHierarchyPath(input.nCore.Graph.gameObject) 
                                                + " in node "+ (input.nCore.Graph.graph as DialogueGraph).GetCurrentChatIndex()
                                                + " in line "+ (input.nCore.Graph.graph as DialogueGraph).indexInLine
                                                +" at symbol " + i + "\n"+ "\n" +printingField.text +"\n"+e);
            }
        }
        printingField.text = line;
        GetComponent<SoundManager>().Stop();
        printCor = null;
    }

    public IEnumerator PrintLineCor(string line)
    {

        Replace(line);
        printingField.text = "";

        GetComponent<SoundManager>().StartAudio(0);
        if(input != null) input.StartInputWaiting();
        yield return printCor = StartCoroutine(PrintCor());

    }



    public void TextEmpty()
    {
        printingField.text = "";
    }
}
