using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DialogueInput : MonoBehaviour
{
    [SerializeField] private DialoguePrinting dPrinting;
    [SerializeField] public DialogueController nCore;

    private KeyCode key = KeyCode.Space, key2 = KeyCode.Return;
    private Coroutine inputCor;
    private bool listening = false;

    public GameObject dialogUI, LogUI;

    public GameObject MenuPrefab;
    private GameObject menu = null;

    public List<GameObject> disableInteractive;
    public List<GameObject> WindowsToClose;
    public List<SoundManager> sm;

    private List<bool> remembered_interacteble = new List<bool>();

    public void SetRemembered()
    {
        int i = 0;
        foreach (var VARIABLE in disableInteractive)
        {
            if (remembered_interacteble.Count > i)
                VARIABLE.GetComponent<CanvasGroup>().interactable = remembered_interacteble[i];
            i++;
        }
    }

    public void remember()
    {
        remembered_interacteble.Clear();
        foreach (var VARIABLE in disableInteractive)
        {
            remembered_interacteble.Add(VARIABLE.GetComponent<CanvasGroup>().interactable);
            VARIABLE.GetComponent<CanvasGroup>().interactable = false;
        }
    }

    public void SetFalse()
    {
        foreach (var VARIABLE in disableInteractive)
        {
            VARIABLE.GetComponent<CanvasGroup>().interactable = false;
        }
    }

    public void CloseAllNotDialogueWindows()
    {
        foreach (var VARIABLE in WindowsToClose)
        {
            if (VARIABLE.activeSelf)
            {
                VARIABLE.SetActive(false);
            }
        }
    }

    public UnityEvent onPause;
    public UnityEvent onUnPause;

    public void SetMenu()
    {
        /*
        if (dPrinting.IsPrinting())
        {
            TryToSkipLine();

            return;
        }

        if (MenuPrefab != null)
        {
            if (menu == null)
            {
                foreach (var VARIABLE in WindowsToClose)
                {
                    if (VARIABLE.activeSelf)
                    {
                        VARIABLE.SetActive(false);
                        return;
                    }
                }

                menu = Instantiate(MenuPrefab);
                onPause.Invoke();
                menu.GetComponent<SettingsViews>().SetNodeInput(this);

                menu.GetComponent<SettingsViews>().ShowMain();
                menu.SetActive(true);

                remembered_interacteble.Clear();
                foreach (var VARIABLE in disableInteractive)
                {
                    remembered_interacteble.Add(VARIABLE.GetComponent<CanvasGroup>().interactable);
                    VARIABLE.GetComponent<CanvasGroup>().interactable = false;
                }
            }
            else
            {
                menu.GetComponent<SettingsViews>().Hide();
                menu.SetActive(false);

                int i = 0;
                foreach (var VARIABLE in disableInteractive)
                {
                    VARIABLE.GetComponent<CanvasGroup>().interactable = remembered_interacteble[i];
                    i++;
                }

                Destroy(menu);
                onUnPause.Invoke();
                menu = null;
            }
        }*/
    }

    private void Update()
    {
        if (!listening && nCore.Graph != null) return;

        if (GameObject.Find("EventSystem") == null || !GameObject.Find("EventSystem").activeSelf) return;
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!dPrinting.IsPrinting())
            {
                SetMenu();

                return;
            }
        }

        if (nCore.Graph == null) return;
        var b = nCore.GetIsInputAwaiting();
        if ((menu == null || !menu.activeSelf) && (dialogUI != null && dialogUI.activeSelf) &&
            (LogUI != null && !LogUI.activeSelf))
            if (Input.GetKeyDown(key) || Input.GetKeyDown(key2) || Input.GetKeyDown(KeyCode.Escape))
                if (!b)
                    TryToSkipLine();
    }

    public void StartInputWaiting()
    {
        listening = true;
    }

    public void StopInputWaiting()
    {
        listening = false;
    }

    private Profile profile = null;

    public void TryToSkipLine()
    {
        if (dPrinting.StopCor()) return;
        if (nCore.nChoices.isActive()) return;
        if (profile == null) profile = GameObject.Find("Profile").GetComponent<Profile>();

        nCore.InvokeOnEnd();
        nCore.NextLine();
    }
}