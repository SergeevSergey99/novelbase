using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DSpeaker : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] private GameObject heroImg, speakObj;
    [SerializeField] private DModel player;
    [SerializeField] private DFocusing dFocusing;
    [SerializeField] private DCharacters dCharacters;
   // [SerializeField] private CharactersNameCollection collection;

   public DCharacters GetCharacters() => dCharacters;
    public void HideSpeaker()
    {
        //heroImg.SetActive(false);
        
        DModel player = GetPlayer();
        if(player != null && player.GetOpacity() > 0.1)
            player.FadeOutAndDeacrive();
        speakObj.SetActive(false);
    }

    DModel GetPlayer()
    {
        if(player != null) player = dCharacters.GetModel("Player");
        return player;
    }

    public void HideSpeakerEnd()
    {
        //heroImg.SetActive(false);
        
        DModel player = GetPlayer();
        if(player != null && player.GetOpacity() > 0.1)
            player.HideModel();
        heroImg.SetActive(false);
    }
    public void ShowSpeaker()
    {
        speakObj.SetActive(true);
    }
    public void Speak(string who)
    {
        if (who.Equals("Player"))
            SpeakHero();
        else
            SpeakOther(who);
    }

    public void StopSpeak()
    {
        speakObj.SetActive(false);
        heroImg.SetActive(false);
        text.text = "";
    }
    private void SpeakHero()
    {
        //player.FadeIn();
        dFocusing.SpeakUnfocus();
        speakObj.SetActive(false);
        if(!heroImg.activeSelf)
           heroImg.SetActive(true);
        text.text = "";

        DModel player = dCharacters.GetModel("Player");
       // if (player.GetOpacity() < 0.95)
       if(player != null) player.FadeIn();
    }

    private DModel lastSpeaker = null;
    private void SpeakOther(string who)
    {
        DModel player = dCharacters.GetModel("Player");
        if(player != null && player.gameObject.activeSelf && player.transform.parent.gameObject.activeSelf && player.GetOpacity() > 0.1)
            player.FadeOut();
        
        speakObj.SetActive(true);
        heroImg.SetActive(false);
        var prf = FindObjectOfType<Profile>();
        text.text = DLineFormatter.Format(prf.gameList.GetCharacter(who).NAME.GetLocalisedString(prf));
        if (who == "Statist" || who == "Autor" || who=="") return;
        lastSpeaker = dCharacters.GetModel(who);
        if(lastSpeaker != null)
            dFocusing.SpeakFocus(lastSpeaker);
    }
}
