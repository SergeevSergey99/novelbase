using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameList : MonoBehaviour
{
    public List<LocationNamePair> Locations = new List<LocationNamePair>();
    public List<Sprite> sprites = new List<Sprite>();
    public List<LocalizedString> days = new List<LocalizedString>();
    public List<Dialogue.CharacterInfo> characterInfos = new List<Dialogue.CharacterInfo>();
    public List<InventoryObjectScriptable> inventoryObjects = new List<InventoryObjectScriptable>();
    public Sprite GetSprite(string spriteName)
    {
        
        var index = sprites.FindIndex(c => c.name.Equals(spriteName));
        if(index == -1)return null;
        return sprites[index];
    }
    public string GetLocationName(string spriteName, Language l)
    {
        var index = Locations.FindIndex(c => c.codeName.name.Equals(spriteName));
        if(index == -1) return "???";
        if(l == Language.EN)
            return (Locations[index].EN_naming);
        return (Locations[index].naming);
        
    }

    public Dialogue.CharacterInfo GetCharacter(string codeName)
    {
        var index = characterInfos.FindIndex(c => c.name.Equals(codeName));
        if(index == -1) return null;
        return characterInfos[index];
    }

}
