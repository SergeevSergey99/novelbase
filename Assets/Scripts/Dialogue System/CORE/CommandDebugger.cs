using System;
using System.Collections;
using System.Collections.Generic;
using Dialogue;
using UnityEngine;
using TMPro;

public class CommandDebugger : MonoBehaviour
{
    public GameObject debugUI;
    public TextMeshProUGUI Graph;
    public TextMeshProUGUI line;
    public TextMeshProUGUI flags;
    public TextMeshProUGUI vars;
    List<string> commands = new List<string>();

    private void Start()
    {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        
        if (debugUI != null) debugUI.SetActive(true);
        if (line != null) line.gameObject.SetActive(true);
        if (Graph != null) Graph.gameObject.SetActive(true);
        
        UpdateFlagsList();
        UpdateVarsList();
#else
        if (debugUI != null) debugUI.SetActive(false);
#endif
    }

    public static string GetHierarchyPath(GameObject go)
    {
        var GraphPath = "";
        
            do
            {
                GraphPath = "/" + go.name + GraphPath;
                if (go.transform.parent == null) break;
                go = go.transform.parent.gameObject;
            } while (go.transform != null);

            return GraphPath;
    }

    public void AddGraph(SceneDialogue g, Profile prf)
    {
        if (Graph == null) return; 
        if (g == null)
            Graph.text = "";
        else
        {
            var p = "";
            if (prf.gameList.GetComponent<PrefabGraphs>().graphsList.Contains(g))
                p = g.name;
            else p = GetHierarchyPath(g.gameObject);
            Graph.text = GetHierarchyPath(g.gameObject) + " " + g.name + " CurrentChatIndex = " +
                         ((DialogueGraph) g.graph).GetCurrentChatIndex() + " " +
                         ((DialogueGraph) g.graph).indexInLine;
        }
    }

    public void AddCommand(string command)
    {
        
        if (line == null) return;
        
        if (commands.Count > 20)
            commands.RemoveAt(0);
        commands.Add(command);
        UpdateCommandList();
    }

    void UpdateCommandList()
    {
        if (line == null) return;
        var txt = "";
        for (int i = 0; i < commands.Count; i++)
        {
            txt += commands[i] + "<br>";
        }

        line.text = txt;
    }

    public void UpdateFlagsList()
    {
        UpdateFlagsList(GameObject.Find("Profile").GetComponent<Profile>());
    }

    public void UpdateFlagsList(Profile prf)
    {
        if (flags == null) return;
        var txt = "";
        foreach (var flag in prf.FlagVariables)
        {
            if (!flag.isRaised)
                txt += "<color=#F00>";
            
            txt += flag.name;
            
            if (!flag.isRaised)
                txt += "</color>";
            
            txt += "\t";
        }

        flags.text = txt;
    }
    public void UpdateVarsList()
    {
        UpdateVarsList(GameObject.Find("Profile").GetComponent<Profile>());
    }

    public void UpdateVarsList(Profile prf)
    {
        if (vars == null) return;
        var txt = "";
        foreach (var variable in prf.intVariables)
        {
            
            txt += variable.name + "<color=#F00>=" + variable.value + "</color>\t";
        }

        vars.text = txt;
    }
}