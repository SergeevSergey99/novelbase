using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DView : MonoBehaviour
{
    [SerializeField] private CanvasGroup cg;
    [SerializeField] private Transform NEXT_BTN;
    [SerializeField] private GameObject SPEAKER_NAME;
    [SerializeField] private GameObject GG_IMAGE;
    [SerializeField] private DSpeaker Ds;
    [SerializeField] private float animTime = .3f;
    
    
    private int id;

    public List<CanvasGroup> DisableIntecactableAtDialogue = new List<CanvasGroup>();
    private List<bool> DisableIntecactableAtDialogue_mem = new List<bool>();
    
    public void InitDialog()
    {
        if(activeCorr != null) StopCoroutine(activeCorr);
        cg.gameObject.SetActive(true);
        cg.gameObject.GetComponent<Image>().raycastTarget = true;

        LeanTween.cancel(id);
        id = LeanTween.alphaCanvas(cg, 1, animTime).id;
        
        NEXT_BTN.GetComponent<Button>().Select();
        
        DisableIntecactableAtDialogue_mem.Clear();
        foreach (var _cg in DisableIntecactableAtDialogue)
        {
            DisableIntecactableAtDialogue_mem.Add(_cg.interactable);
            _cg.interactable = false;
        }
    }

    public bool ShowDialogue()
    {
        if (!cg.gameObject.activeSelf || DisableIntecactableAtDialogue_mem.Count < DisableIntecactableAtDialogue.Count)
        {
            InitDialog();
            return true;
        }

        return false;
    }

    private Coroutine activeCorr = null;
    public void HideDialog()
    {
        Ds.HideSpeaker();
        LeanTween.cancel(id);
        id = LeanTween.alphaCanvas(cg, 0, animTime).id;
        
        
        cg.gameObject.GetComponent<Image>().raycastTarget = false;
        if(activeCorr != null) StopCoroutine(activeCorr);
        activeCorr = StartCoroutine(deactiving());

        int i = 0;
        foreach (var cg in DisableIntecactableAtDialogue)
        {
            if(i < DisableIntecactableAtDialogue_mem.Count)
                cg.interactable = DisableIntecactableAtDialogue_mem[i];
            i++;
        }
    }

    public void HideUI()
    {
        LeanTween.alphaCanvas(cg, 0, animTime);
        cg.interactable = false;
        
        SPEAKER_NAME.SetActive(false);
            
        if(GG_IMAGE.activeSelf && GG_IMAGE.transform.childCount > 0)
            GG_IMAGE.transform.GetChild(0).GetComponent<DModel>().FadeOut();
        foreach (var cs in DisableIntecactableAtDialogue)
        {
            LeanTween.alphaCanvas(cs, 0, animTime);
            
        }

        
    }
    public void ShowUI()
    {
        LeanTween.alphaCanvas(cg, 1, animTime);
        cg.interactable = true;

        SPEAKER_NAME.SetActive(true);
        if(GG_IMAGE.transform.childCount > 0)
            GG_IMAGE.transform.GetChild(0).GetComponent<DModel>().FadeIn();
        foreach (var cs in DisableIntecactableAtDialogue)
        {
            LeanTween.alphaCanvas(cs, 1, animTime);
            
        }

        
    }

    IEnumerator deactiving()
    {
        yield return new WaitForSeconds(animTime);
        cg.gameObject.SetActive(false);
    }
    IEnumerator activing(GameObject obj, bool b)
    {
        yield return new WaitForSeconds(animTime);
        obj.SetActive(b);
    }
}
