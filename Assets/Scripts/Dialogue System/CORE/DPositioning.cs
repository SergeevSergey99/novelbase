using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DPositioning : MonoBehaviour
{
    [SerializeField] private Camera cam;
    [SerializeField] private List<GameObject> screenObjects = new List<GameObject>();
    [SerializeField] private List<GameObject> AltScreenObjects = new List<GameObject>();
    [SerializeField] private GameObject zoomObj;
    public List<Vector3> positions = new List<Vector3>();
    private List<Vector3> AltPositions = new List<Vector3>();
    private Vector3 zoomPos;

    private void Awake()
    {
        if(cam == null) cam = Camera.main;
        if(cam == null) cam = Camera.current;
/*        zoomPos = cam.ScreenToWorldPoint(zoomObj.transform.position);
        foreach (var item in screenObjects)
            if(!positions.Contains(cam.ScreenToWorldPoint(item.transform.position)))
                positions.Add(cam.ScreenToWorldPoint(item.transform.position));
        foreach (var item in AltScreenObjects)
            if(!AltPositions.Contains(cam.ScreenToWorldPoint(item.transform.position)))
                AltPositions.Add(cam.ScreenToWorldPoint(item.transform.position));*/
        zoomPos = (zoomObj.transform.position);
        foreach (var item in screenObjects)
            if(!positions.Contains((item.transform.position)))
                positions.Add((item.transform.position));
        foreach (var item in AltScreenObjects)
            if(!AltPositions.Contains((item.transform.position)))
                AltPositions.Add((item.transform.position));
    }

    private void Start()
    {
        positions.Clear();
        AltPositions.Clear();
        Awake();
    }

    public Vector3 GetPos(int id)
    {
        if(positions.Count == 0) Awake();
        return positions[id + 2];
    }
    public Vector3 GetAltPos(int id)
    {
        if(positions.Count == 0) Awake();
        return positions[id];
    }

    public Vector3 GetZoomPos()
    {
        if(positions.Count == 0) Awake();
        return zoomPos;
    }
}