using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class JsonHelper
{
    public static MyWrapper ParseJsonToObject(string json)
    {
        var wrappedjsonArray = JsonUtility.FromJson<MyWrapper>(json);
        foreach (var item in wrappedjsonArray.objects)
        {
            if (item.Line_Rus == null)
                item.Line_Rus = "";
            if (item.Line_Eng == null)
                item.Line_Eng = "";
            if (item.WhoTalk == null)
                item.WhoTalk = "";
            if (item.Tags == null)
                item.Tags = "";
            if (item.OnEnd == null)
                item.OnEnd = "";
            if (item.Expressions == null)
                item.Expressions = "";

        }
        return wrappedjsonArray;
    }

    [Serializable]
    public class MyWrapper
    {
        public List<DLines> objects;
    }
}
