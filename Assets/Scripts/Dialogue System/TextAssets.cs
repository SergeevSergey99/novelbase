using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextAssets : MonoBehaviour
{
    [SerializeField] private List<TextAsset> textAssets = new List<TextAsset>();
    private Dictionary<string, TextAsset> textAssetsDic = new Dictionary<string, TextAsset>();

    //[ExecuteInEditMode]
    private void Awake()
    {
        foreach (var item in textAssets)
            if(!textAssetsDic.ContainsKey(item.name))
                textAssetsDic.Add(item.name, item);
    }

    public TextAsset GetTA(string key)
    {
        if(!textAssetsDic.ContainsKey(key))
            Awake();
        return textAssetsDic[key];
    }
    public List<DLines> Read(string taName)
    {
        var x = JsonHelper.ParseJsonToObject(GetTA(taName).text);
        return x.objects;
    }
}
