using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapInput : MonoBehaviour
{
    [SerializeField] private BackGroundAnimation backGround;
    private KeyCode key = KeyCode.Space, key2 = KeyCode.Return;
    private Coroutine inputCor;
    private bool listening = false;

    private void Update()
    {
        if (!listening) return;
        if (Input.GetKeyDown(key) || Input.GetKeyDown(key2))
            ContinueSwap();
    }

    public void StartInputWaiting()
    {
        listening = true;
    }

    public void StopInputWaiting()
    {
        listening = false;
    }

    public void ContinueSwap()
    {
        StopInputWaiting();
        backGround.ContinueSwap();
    }
}