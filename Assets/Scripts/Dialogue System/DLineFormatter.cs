using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DLineFormatter : MonoBehaviour
{
    public static string nameChanger = "%name%";

    public static string Format(string line)
    {
        if (GameObject.Find("Profile") == null) return line.Replace(nameChanger, "Hero");
        line = line.Replace(nameChanger, GameObject.Find("Profile").GetComponent<Profile>().heroName);
        return line;
    }
    public static string Format(string line, Profile prf)
    {
        if (prf == null) return line.Replace(nameChanger, "Hero");
        line = line.Replace(nameChanger, prf.heroName);
        return line;
    }
}
