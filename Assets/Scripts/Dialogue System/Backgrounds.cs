using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Backgrounds : MonoBehaviour
{
    [NonSerialized]
    private GameList gameList = null;
    //[SerializeField] private List<Sprite> sprites = new List<Sprite>();
    private Dictionary<string, Sprite> spriteDic = new Dictionary<string, Sprite>();

    public GameList getBack()
    {
        if (gameList == null)
        {
            gameList = FindObjectOfType<Profile>().gameList;
            Awake();
        }

        return gameList;
    }

    //[ExecuteInEditMode]
    private void Awake()
    {
        getBack();

        spriteDic.Clear();
        foreach (var item in gameList.sprites)
            if(!spriteDic.ContainsKey(item.name))
                spriteDic.Add(item.name, item);
    }

    public Sprite GetSprite(string spriteName)
    {
      if(spriteDic.Count == 0)Awake();
        return spriteDic[spriteName];
    }
}
