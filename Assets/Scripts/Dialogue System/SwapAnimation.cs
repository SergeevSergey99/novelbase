using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwapAnimation : MonoBehaviour
{
    public CanvasGroup cg;
    public Image im;
    public Sprite day, evening, night;
    private float anim = .9f;
    private List<string> animationList = new List<string>();
    public List<GameObject> deactiveList;// = new List<GameObject>();
    private float WaitTime = 0.01f;

    private bool awaked = false;
    private void Awake()
    {
        awaked = true;
        var es = GameObject.Find("EventSystem");
        if(!deactiveList.Contains(es) && es != null) deactiveList.Add(es);
        animationList.Add("day_to_night");
        animationList.Add("night_to_day");
    }

    public bool IsAnimation(string animName)
    {
        Debug.Log(animationList.Contains(animName));
        return animationList.Contains(animName);
    }

    public void Deactive()
    {
        if(!awaked)Awake();
        foreach (var VARIABLE in deactiveList)
        {
            VARIABLE.SetActive(false);
        }
    }
    public void Active()
    {
        foreach (var VARIABLE in deactiveList)
        {
            VARIABLE.SetActive(true);
        }
    }

    public IEnumerator TryToPlayAnim(string animName)
    {
        if (animationList.Contains(animName))
        {
            switch (animName)
            {
                case "day_to_night":
                    yield return StartCoroutine(DayToNight());
                    break;

                case "night_to_day":
                    yield return StartCoroutine(NightToDay());
                    break;
                default:
                    break;
            }
        }
    }

    public IEnumerator DayToNight()
    {
        Deactive();
        cg.alpha = 1;
        im.sprite = day;
        //for (float i = 0; i < anim * 2; i+=WaitTime)
        {
            yield return new WaitForSeconds(anim * 2);

        }
        im.sprite = evening;
        yield return new WaitForSeconds(anim);
        im.sprite = night;
        yield return new WaitForSeconds(anim);
        cg.alpha = 0;
        Active();
    }

    public IEnumerator NightToDay()
    {
        Deactive();
        cg.alpha = 1;
        im.sprite = night;
        yield return new WaitForSeconds(anim * 2);
        im.sprite = evening;
        yield return new WaitForSeconds(anim);
        im.sprite = day;
        yield return new WaitForSeconds(anim);
        cg.alpha = 0;
        Active();
    }
}
