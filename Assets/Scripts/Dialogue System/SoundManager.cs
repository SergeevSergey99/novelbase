using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public enum soundManagerType
{
    Music,
    Effect
};

[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviour
{
    public List<AudioClip> AudioClips;
    [SerializeField] private float typeTime = .04f;

    private Coroutine audioCor;

    //private AudioSource source = null;
    //private bool canChange = true;
    private float maxMusic = 1;
    public int defaultSound = -1;
    public soundManagerType type = soundManagerType.Music;

    private void Start()
    {
        p = GameObject.Find("Profile").GetComponent<Profile>();
        if (p == null) return;
        maxMusic = (type == soundManagerType.Music
                ? p.Sound_Music
                : p.Sound_Effects)
            / 100.0f * p.Sound_General_sound / 100.0f;
        GetComponent<AudioSource>().volume = maxMusic;
        if (defaultSound >= 0) StartAudio(AudioClips[defaultSound].name);
    }

    private Profile p;

    public void Pause()
    {
        GetComponent<AudioSource>().Pause();
    }

    public void unPause()
    {
        if (gameObject.activeSelf == false || p == null)
            return;

        CalcVolume();

        GetComponent<AudioSource>().Play();
    }

    public void CalcVolume()
    {
        if (p == null) p = GameObject.Find("Profile").GetComponent<Profile>();

        maxMusic = (type == soundManagerType.Music ? p.Sound_Music : p.Sound_Effects) / 100.0f * p.Sound_General_sound /
                   100.0f;
        GetComponent<AudioSource>().volume = maxMusic;
    }

    public void SetAudio(string name)
    {
        foreach (var item in AudioClips)
        {
            if (item.name.Equals(name))
            {
                GetComponent<AudioSource>().volume = maxMusic;
                GetComponent<AudioSource>().clip = item;
                GetComponent<AudioSource>().enabled = true;
                GetComponent<AudioSource>().Play();

                return;
            }
        }
    }

    public void StartAudio(string name)
    {
        foreach (var item in AudioClips)
        {
            if (item == null) continue;
            if (item.name.Equals(name))
            {
                if (GetComponent<AudioSource>().clip == null)
                {
                    GetComponent<AudioSource>().volume = 0;
                    GetComponent<AudioSource>().clip = item;
                    GetComponent<AudioSource>().enabled = true;
                    GetComponent<AudioSource>().Play();
                    audioCor = StartCoroutine(StartLound());
                }
                else
                {
                    audioCor = StartCoroutine(ResetLound(item));
                }

                return;
            }
        }
    }

    public void StartAudio(int i)
    {
        if (AudioClips.Count == 0 || AudioClips[0] == null) return;
        GetComponent<AudioSource>().volume = 0;
        GetComponent<AudioSource>().clip = AudioClips[i];
        GetComponent<AudioSource>().enabled = true;
        GetComponent<AudioSource>().Play();
        audioCor = StartCoroutine(StartLound());
    }

    public void StopAudio()
    {
        StartCoroutine(StopLound());
    }

    IEnumerator StartLound()
    {
        //canChange = false;
        WaitForSeconds typeWait = new WaitForSeconds(typeTime);
        for (int i = 0; GetComponent<AudioSource>().volume < maxMusic; i++)
        {
            GetComponent<AudioSource>().volume += typeTime;
            yield return typeWait;
        }

        audioCor = null;
    }

    public void Stop()
    {
        GetComponent<AudioSource>().Stop();
        GetComponent<AudioSource>().clip = null;
    }

    IEnumerator StopLound()
    {
        WaitForSeconds typeWait = new WaitForSeconds(typeTime);
        for (int i = 0; GetComponent<AudioSource>().volume > 0.05; i++)
        {
            GetComponent<AudioSource>().volume -= typeTime;
            yield return typeWait;
        }

        GetComponent<AudioSource>().volume = 0;
        GetComponent<AudioSource>().enabled = false;
    }

    IEnumerator ContinueLound()
    {
        GetComponent<AudioSource>().enabled = true;
        WaitForSeconds typeWait = new WaitForSeconds(typeTime);
        for (int i = 0; GetComponent<AudioSource>().volume < maxMusic; i++)
        {
            GetComponent<AudioSource>().volume += typeTime;
            yield return typeWait;
        }

        GetComponent<AudioSource>().volume = maxMusic;
    }

    IEnumerator ResetLound(AudioClip sound)
    {
        WaitForSeconds typeWait = new WaitForSeconds(typeTime);
        for (int i = 0; GetComponent<AudioSource>().volume > 0.05; i++)
        {
            GetComponent<AudioSource>().volume -= typeTime;
            yield return typeWait;
        }

        GetComponent<AudioSource>().volume = 0;
        GetComponent<AudioSource>().Stop();
        GetComponent<AudioSource>().enabled = true;
        GetComponent<AudioSource>().clip = sound;
        GetComponent<AudioSource>().Play();


        for (int i = 0; GetComponent<AudioSource>().volume < maxMusic; i++)
        {
            GetComponent<AudioSource>().volume += typeTime;
            yield return typeWait;
        }

        GetComponent<AudioSource>().volume = maxMusic;

        audioCor = null;
    }
}