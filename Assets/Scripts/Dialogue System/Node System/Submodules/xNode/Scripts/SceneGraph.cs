﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using XNode;

namespace XNode {
	/// <summary> Lets you instantiate a node graph in the scene. This allows you to reference in-scene objects. </summary>
	[ExecuteInEditMode]
	public class SceneGraph : MonoBehaviour {
		public NodeGraph graph;
		
		// Detects commands executed and prints them.
#if UNITY_EDITOR
		//catch duplication of this GameObject
		
		void Awake()
		{
			
			if (Application.isPlaying || graph == null)
				return;
			
			graph = graph.Copy();
			graph.name = graph.name.Replace("(Clone)", "");
			for (int i =0; i < graph.nodes.Count; i++)
			{
				string str = graph.nodes[i].name.Replace("(Clone)", "");

				graph.nodes[i].name = str;
			}
		}
#endif

    }

	/// <summary> Derive from this class to create a SceneGraph with a specific graph type. </summary>
	/// <example>
	/// <code>
	/// public class MySceneGraph : SceneGraph<MyGraph> {
	///
	/// }
	/// </code>
	/// </example>
	public class SceneGraph<T> : SceneGraph where T : NodeGraph {
		public new T graph { get { return base.graph as T; } set { base.graph = value; } }
    }
}