﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using XNode;

namespace Dialogue
{
    [CreateAssetMenu(menuName = "Graph/Dialogue/Graph", order = 0)]
    public class DialogueGraph : NodeGraph
    {
        [HideInInspector] public Chat current;

        [HideInInspector] public Node currNode;

        private DialogueController DC = null;

        public DialogueController DController()
        {
            if(DC == null) DC = FindObjectOfType<DialogueController>();
            return DC;
        }

        public void Restart()
        {
            //Find the first DialogueNode without any inputs. This is the starting node.
            if (nodes.Find(x => x is DialogueBaseNode && x.Inputs.All(y => !y.IsConnected)) is Chat)
                current = nodes.Find(x => x is DialogueBaseNode && x.Inputs.All(y => !y.IsConnected)) as Chat;
            else
            {
                DialogueBaseNode n =
                        (nodes.Find(x =>
                            x is DialogueBaseNode && x.Inputs.All(y => !y.IsConnected)) as DialogueBaseNode)
                    ;
                n.Trigger();
            }
        }

        public int GetCurrentChatIndex()
        {
            int i = -1;
            foreach (var VARIABLE in nodes)
            {
                i++;

                if (VARIABLE == currNode)
                    break;
            }

            return i;
        }

        public void SetCurrentChatIndex(int i)
        {
            if (nodes.Count == 0) return;
            if (nodes.Count <= i) SetCurrentChatIndex(i - 1);
            else
            {
                if (nodes[i].GetType() == typeof(Chat))
                {
                    (nodes[i] as Chat).Trigger();
                }
                else if (nodes[i].GetType() == typeof(DialogueLine))
                {
                    (nodes[i] as DialogueLine).Trigger();
                }
            }
        }

        public int indexInLine;


        public bool isLast()
        {
            return current.Outputs.All(y => !y.IsConnected);
        }

        public bool IsAnswerOutputExist(int i)
        {
            return (current as Chat).IsAnswerOutputExist(i);
        }

        public Chat AnswerQuestion(int i)
        {
            (current as Chat).AnswerQuestion(i);
            return (current as Chat);
        }
    }
}