﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace Dialogue
{
    [NodeTint("#AA4455")]
    public class RandomNode : DialogueBaseNode
    {
        [System.Serializable]
        public class RandomLine
        {
            public int weight;
        }

        [Output] public DialogueBaseNode pass;
        [Output(instancePortList = true)] public List<RandomLine> randomLines = new List<RandomLine>();

        //public float time = 0;

        public override void Trigger()
        {
            (graph as DialogueGraph).currNode = this;

            //Trigger next nodes
            NodePort port = null;
            if (randomLines.Count == 0)
                port = GetOutputPort("pass");
            else
            {
                int sum = 0;
                foreach (var VARIABLE in randomLines)
                    sum += VARIABLE.weight;

                var rnd = Random.Range(1, sum + 1);

                Debug.Log("rand = " + rnd);
                int i = 0;
                foreach (var VARIABLE in randomLines)
                {
                    if (VARIABLE.weight < rnd)
                    {
                        rnd -= VARIABLE.weight;
                    }
                    else
                    {
                        port = GetOutputPort("randomLines " + i);
                        break;
                    }

                    i++;
                }
            }

            if (port == null)
            {
                return;
            }

            if (port.ConnectionCount == 0)
            {
                (graph as DialogueGraph).current = null;
            }

            for (int i = 0; i < port.ConnectionCount; i++)
            {
                NodePort connection = port.GetConnection(i);
                (connection.node as DialogueBaseNode).Trigger();
            }
        }
    }
}