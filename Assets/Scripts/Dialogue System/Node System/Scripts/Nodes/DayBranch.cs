﻿using System;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace Dialogue
{
    
    [Node.NodeTint("#DC582A")]
    public class DayBranch : DialogueBaseNode
    {
        [Output] public DialogueBaseNode Monday;
        [Output] public DialogueBaseNode Tuesday;
        [Output] public DialogueBaseNode Wednesday;
        [Output] public DialogueBaseNode Thursday;
        [Output] public DialogueBaseNode Friday;
        [Output] public DialogueBaseNode Saturday;
        [Output] public DialogueBaseNode Sunday;

        public override void Trigger()
        {
            
            Profile prf = GameObject.Find("Profile").GetComponent<Profile>();
            var day = prf._day;

            NodePort port = null;

            switch (day)
            {
                case 1: port = GetOutputPort("Monday"); break;
                case 2: port = GetOutputPort("Tuesday"); break;
                case 3: port = GetOutputPort("Wednesday"); break;
                case 4: port = GetOutputPort("Thursday"); break;
                case 5: port = GetOutputPort("Friday"); break;
                case 6: port = GetOutputPort("Saturday"); break;
                case 7: port = GetOutputPort("Sunday"); break;
            }
            
            if (port == null) return;
            for (int i = 0; i < port.ConnectionCount; i++)
            {
                NodePort connection = port.GetConnection(i);
                (connection.node as DialogueBaseNode).Trigger();
            }
            
        }
    }
}