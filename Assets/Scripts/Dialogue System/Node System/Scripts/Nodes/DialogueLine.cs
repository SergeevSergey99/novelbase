using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace Dialogue
{
    [NodeTint("#CC8888")]
    public class DialogueLine : DialogueBaseNode
    {
        [Output] public DialogueBaseNode LineOutput;

        public bool ReadFromFile = true;

        public TextAssets textAssets;
        public string TextAssetFile;
        public TextAsset FILE;
        
        public string fileName = "";


        public List<ChatLine> Replicas = new List<ChatLine>();
        [SerializeField]
        private int i = 0;

        [Output(instancePortList = true)] public List<Chat.Answer> answers = new List<Chat.Answer>();
        [Output(instancePortList = true)] public List<Chat.Answer> answersRight = new List<Chat.Answer>();

        
        [System.Serializable]
        public class ChatLine
        {
            //public CharacterInfo character;
            public CharacterInfo WhoTalk;

            [TextArea] public string text;
            [TextArea] public string textEN;

            public string Tags;
            public string OnEnd;
            public string Expressions;

            //public double SpeechSpeed = 2.5;
            //public bool isPortretShown = true;
        }

        public void SetI(int _i)
        {
            i = _i;
            
            Trigger();
            
        }
        private List<DLines> fileReplicas = new List<DLines>();

        public int GetCount()
        {
            if (ReadFromFile == false)
                return Replicas.Count;
            return fileReplicas.Count;
        }

        private List<Chat> _chats = new List<Chat>();
        public override void Trigger()
        {
            if (i < 0) i = 0;
            (graph as DialogueGraph).currNode = this;
            if (ReadFromFile && i == 0)
            {
                fileReplicas.Clear();
                if(FILE == null)
                    fileReplicas = new List<DLines>(textAssets.Read(TextAssetFile));
                else
                {
                    fileReplicas = new List<DLines>(JsonHelper.ParseJsonToObject(FILE.text).objects);
                }
            }

            if (i >= GetCount())
            {
                
                NodePort port = GetOutputPort("LineOutput");

                if (port == null) return;
                i = 0;
                (graph as DialogueGraph).indexInLine = 0;
                if (port.ConnectionCount == 0)
                {
                    (graph as DialogueGraph).current = null;
                    return;
                }

                foreach (var VARIABLE in _chats)
                {
                    
                    GetPort("input").Disconnect(VARIABLE.GetOutputPort("output"));
                    ScriptableObject.Destroy(VARIABLE);
                }
                _chats.Clear();
                
                
                for (int j = 0; j < port.ConnectionCount; j++)
                {
                    NodePort connection = port.GetConnection(j);
                    (connection.node as DialogueBaseNode).Trigger();
                }


                return;
            }

            Chat c = ScriptableObject.CreateInstance<Chat>();

            if (ReadFromFile == false)
            {
                c.WhoTalk = Replicas[i].WhoTalk;
                c.text = Replicas[i].text;
                c.textEN = Replicas[i].textEN;
                c.Tags = Replicas[i].Tags;
                c.OnEnd = Replicas[i].OnEnd;
                c.Expressions = Replicas[i].Expressions;
                c.graph = graph;
            }
            else
            {
                c.WhoTalk = (graph as DialogueGraph).DController().PROFILE().gameList.GetCharacter(fileReplicas[i].WhoTalk);
                c.text = fileReplicas[i].Line_Rus;
                c.textEN = fileReplicas[i].Line_Eng;
                c.Tags = fileReplicas[i].Tags;
                c.OnEnd = fileReplicas[i].OnEnd;
                c.Expressions = fileReplicas[i].Expressions;
                c.graph = graph;
            }

            c.answers = new List<Chat.Answer>();
            c.answersRight = new List<Chat.Answer>();

            NodePort Cport = c.GetOutputPort("output");
            NodePort CPort = GetOutputPort("LineOutput");

            (graph as DialogueGraph).current = c;
            (graph as DialogueGraph).indexInLine = i;
            if (i >= GetCount() - 1)
            {
                i = -1;
                if (c.text.StartsWith("choice["))
                {
                    var newStr = c.text.Split("}|right|{");
                    var newStrEN = c.textEN.Split("}|right|{");

                    char[] spearator = {']', '{', '}'};
                    string[] choices = newStr[0].Split(spearator);
                    string[] choicesEN = newStrEN[0].Split(spearator);

                    c.text = choices[0].Substring("choice[".Length);
                    c.textEN = choicesEN[0].Substring("choice[".Length);

                    int conditionIndex = 0;
                    for (int j = 1; j < choices.Length; j++)
                    {
                        if (choices[j].Equals("")) continue;

                        Chat.Answer ca = new Chat.Answer();
                        ca.text = choices[j];
                        ca.textEN = choicesEN[j];
                        ca.conditions = answers[conditionIndex].conditions;
                        conditionIndex++;

                        c.answers.Add(ca);
                        var toConnect = this.GetOutputPort("answers " + (c.answers.Count - 1));
                        var fromConnect = c.AddDynamicOutput(typeof(DialogueBaseNode), ConnectionType.Override,
                            TypeConstraint.None, "answers " + (c.answers.Count - 1));
                        if (toConnect != null && toConnect.Connection != null)
                            fromConnect.Connect(toConnect.Connection);
                    }

                    if (newStr.Length > 1)
                    {
                        conditionIndex = 0;
                        string[] Rchoices = newStr[1].Split(spearator);
                        string[] RchoicesEN = newStrEN[1].Split(spearator);
                        for (int j = 0; j < Rchoices.Length; j++)
                        {
                            if (Rchoices[j].Equals("")) continue;

                            Chat.Answer ca = new Chat.Answer();
                            ca.text = Rchoices[j];
                            ca.textEN = RchoicesEN[j];
                            if (answersRight.Count > conditionIndex)
                                ca.conditions = answersRight[conditionIndex].conditions;
                            else
                                ca.conditions = Array.Empty<Condition>();
                            conditionIndex++;

                            c.answersRight.Add(ca);
                            var toConnect = this.GetOutputPort("answersRight " + (c.answersRight.Count - 1));
                            var fromConnect = c.AddDynamicOutput(typeof(DialogueBaseNode), ConnectionType.Override,
                                TypeConstraint.None, "answersRight " + (c.answersRight.Count - 1));
                            if (toConnect != null && toConnect.Connection != null)
                                fromConnect.Connect(toConnect.Connection);
                        }
                    }
                }
                else
                {
                    Cport.Connect(CPort.Connection);
                }

                (graph as DialogueGraph).indexInLine = 0;
                foreach (var VARIABLE in _chats)
                {
                    GetPort("input").Disconnect(VARIABLE.GetOutputPort("output"));
                    ScriptableObject.Destroy(VARIABLE);
                }

                _chats.Clear();
            }
            else
                Cport.Connect(this.GetPort("input"));

            _chats.Add(c);
            i++;
        }
    }
}