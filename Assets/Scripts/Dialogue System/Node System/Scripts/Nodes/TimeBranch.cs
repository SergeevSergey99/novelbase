﻿using System;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace Dialogue
{
    [System.Serializable] public class TimePeriod
    {
        public int HourGreaterEqualThan;
        public int HourLessThan;
    }
    
    [Node.NodeTint("#CCCCFF")]
    public class TimeBranch : DialogueBaseNode
    {
        [Output(instancePortList = true)] public List<TimePeriod> periods = new List<TimePeriod>();

        public override void Trigger()
        {
            
            Profile prf = GameObject.Find("Profile").GetComponent<Profile>();
            var time = prf._time.Split(':');
            int hour = Int32.Parse(time[0]);
            int minutes = Int32.Parse(time.Length > 1 ?time[1] : "00");

            int index = 0;
            int i = 0;
            foreach (var period in periods)
            {
                if (period.HourGreaterEqualThan <= hour && hour < period.HourLessThan)
                {
                    index = i;
                    break;
                }
                i++;
            }
            
            NodePort port = null;
            if (periods.Count == 0) {
                port = GetOutputPort("output");
            } else {
                if (periods.Count <= index) return;
                port = GetOutputPort("periods " + index);
            }
            
            if (port == null) return;
            for (i = 0; i < port.ConnectionCount; i++)
            {
                NodePort connection = port.GetConnection(i);
                (connection.node as DialogueBaseNode).Trigger();
            }
            
        }
    }
}