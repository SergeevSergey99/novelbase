﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.Events;
using XNode;

namespace Dialogue
{
    [NodeTint("#CCCCFF")]
    public class Branch : DialogueBaseNode
    {
        public enum ConditionStatus
        {
            ByCondition,
            AlwaysTrue,
            AlwaysFalse,
            ByNotCondition,
            BreakGraph
        }

        public ConditionStatus conditionStatus;

        public Condition[] conditions;
        [Output] public DialogueBaseNode pass;
        [Output] public DialogueBaseNode fail;

        private bool success;

        public override void Trigger()
        {
            (graph as DialogueGraph).currNode = this;
            if (conditionStatus == ConditionStatus.BreakGraph) return;
            // Perform condition
            bool success = true;
            if (conditionStatus == ConditionStatus.ByCondition || conditionStatus == ConditionStatus.ByNotCondition)
            {
                for (int i = 0; i < conditions.Length; i++)
                {
                    if (!conditions[i].Invoke())
                    {
                        success = false;
                        break;
                    }
                }
            }
            else if (conditionStatus == ConditionStatus.AlwaysFalse) 
                success = false;

            if (conditionStatus == ConditionStatus.ByNotCondition) 
                success = !success;

            //Trigger next nodes
            NodePort port;
            if (success) port = GetOutputPort("pass");
            else port = GetOutputPort("fail");
            if (port == null || port.ConnectionCount == 0)
            {
                (graph as DialogueGraph).currNode = null;
                (graph as DialogueGraph).current = null;
                return;
            }

            for (int i = 0; i < port.ConnectionCount; i++)
            {
                NodePort connection = port.GetConnection(i);
                if (connection == null)
                {
                    (graph as DialogueGraph).currNode = null;
                    (graph as DialogueGraph).current = null;
                    return;
                }

                (connection.node as DialogueBaseNode).Trigger();
            }
        }
    }

    [Serializable]
    public class Condition : SerializableCallback<bool>
    {
    }
}