﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace Dialogue
{
    [NodeTint("#44AA55")]
    public class Point : DialogueBaseNode
    {
       /* public SerializableEvent[]
            trigger = new SerializableEvent[1];// Could use UnityEvent here, but UnityEvent has a bug that prevents it from serializing correctly on custom EditorWindows. So i implemented my own.
       */
        [Output] public DialogueBaseNode pass;
        //public float time = 0;

        public override void Trigger()
        {
            (graph as DialogueGraph).currNode = this;
            
            //Trigger next nodes
            NodePort port;
            port = GetOutputPort("pass");
            if (port == null)
            {
                return;
            }

            if (port.ConnectionCount == 0)
            {
                (graph as DialogueGraph).current = null;
            }
            
            for (int i = 0; i < port.ConnectionCount; i++)
            {
                NodePort connection = port.GetConnection(i);
                (connection.node as DialogueBaseNode).Trigger();
            }
        }

    }
}