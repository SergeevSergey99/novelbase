﻿using System.Collections.Generic;
using System.Linq;
using Dialogue;
using UnityEditor;
using UnityEngine;
using XNode;
using XNodeEditor;

namespace DialogueEditor {
    [CustomNodeEditor(typeof(DayBranch))]
    public class DayBranchEditor : NodeEditor {

        public override void OnBodyGUI() {
            serializedObject.Update();

            DayBranch node = target as DayBranch;
            if (NodeEditorWindow.current.zoom > 3)
            {
                serializedObject.ApplyModifiedProperties();
                return;
            }
            NodeEditorGUILayout.PortField(GUIContent.none, target.GetInputPort("input"));
            
            GUILayout.Space(-10);

            NodeEditorGUILayout.PortField(target.GetOutputPort("Monday"));
            NodeEditorGUILayout.PortField(target.GetOutputPort("Tuesday"));
            NodeEditorGUILayout.PortField(target.GetOutputPort("Wednesday"));
            NodeEditorGUILayout.PortField(target.GetOutputPort("Thursday"));
            NodeEditorGUILayout.PortField(target.GetOutputPort("Friday"));
            NodeEditorGUILayout.PortField(target.GetOutputPort("Saturday"));
            NodeEditorGUILayout.PortField(target.GetOutputPort("Sunday"));
      
            
            serializedObject.ApplyModifiedProperties();
        }

        public override int GetWidth() {
            return 100;
        }
    }
}