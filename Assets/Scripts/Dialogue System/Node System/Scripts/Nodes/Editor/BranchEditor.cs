﻿using System.Collections.Generic;
using System.Linq;
using Dialogue;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

namespace DialogueEditor {
    [CustomNodeEditor(typeof(Branch))]
    public class BranchEditor : NodeEditor {
        private static GUIStyle editorLabelStyle;
        public override void OnBodyGUI() {
            serializedObject.Update();

            if (editorLabelStyle == null) editorLabelStyle = new GUIStyle(EditorStyles.label);
            
            EditorStyles.label.normal.textColor = Color.black;
            Branch node = target as Branch;
            if (NodeEditorWindow.current.zoom > 3)
            {
                serializedObject.ApplyModifiedProperties();
                return;
            }
            NodeEditorGUILayout.PortField(target.GetInputPort("input"));
            EditorGUILayout.Space();
            //base.OnBodyGUI();
            EditorStyles.label.normal = editorLabelStyle.normal;
            NodeEditorGUILayout.PropertyField(serializedObject.FindProperty("conditionStatus"));
            NodeEditorGUILayout.PropertyField(serializedObject.FindProperty("conditions"));
            
            
            EditorStyles.label.normal.textColor = Color.black;
            NodeEditorGUILayout.PortField(target.GetOutputPort("pass"));
            NodeEditorGUILayout.PortField(target.GetOutputPort("fail"));

            serializedObject.ApplyModifiedProperties();
            //base.OnBodyGUI();
            EditorStyles.label.normal = editorLabelStyle.normal;
            
        }

        public override int GetWidth() {
            return 336;
        }
    }
}