using System;
using System.Collections.Generic;
using System.Linq;
using Dialogue;
using UnityEditor;
using UnityEngine;
using XNode;
using XNodeEditor;

namespace DialogueEditor {
    [CustomNodeEditor(typeof(DialogueLine))]
    public class DialogueLineEditor : NodeEditor
    {
        private List<DLines> dlines = new List<DLines>();
        public override void OnBodyGUI() {
            serializedObject.Update();

            DialogueLine node = target as DialogueLine;
            if (NodeEditorWindow.current.zoom > 3)
            {
                serializedObject.ApplyModifiedProperties();
                return;
            }
            
            NodeEditorGUILayout.PortField(GUIContent.none, target.GetInputPort("input"));
            
            
            if(node.answers.Count + node.answersRight.Count == 0)
                NodeEditorGUILayout.PortField(target.GetOutputPort("LineOutput"));
            GUILayout.Space(10);
            
            
            NodeEditorGUILayout.PropertyField(serializedObject.FindProperty("ReadFromFile"));

            if (node.ReadFromFile == false)
            {
                NodeEditorGUILayout.PropertyField(serializedObject.FindProperty("Replicas"));
            }
            else
            {
                NodeEditorGUILayout.PropertyField(serializedObject.FindProperty("textAssets"));
                NodeEditorGUILayout.PropertyField(serializedObject.FindProperty("TextAssetFile"));
                NodeEditorGUILayout.PropertyField(serializedObject.FindProperty("FILE"));

                
                if (node.FILE!= null && (node.FILE.name != node.fileName))
                {
                    node.fileName = node.FILE.name;
                    node.answers.Clear();
                    node.answersRight.Clear();
                    dlines.Clear();
                    try
                    {
                        dlines = new List<DLines>(JsonHelper.ParseJsonToObject(node.FILE.text).objects);

                    }
                    catch (Exception e)
                    {
                        Debug.Log("Json problem in " + node.FILE.name +"\n" + e);
                    }

                    if (dlines[dlines.Count - 1].Line_Rus.StartsWith("choice["))
                    {
                        //node.answers.Clear();
 
                        var newStr = dlines[dlines.Count - 1].Line_Rus.Split("}|right|{");
                        var newStrEN = dlines[dlines.Count - 1].Line_Eng.Split("}|right|{");
                        
                        char[] spearator = {']','{','}'};

                        string[] choices = newStr[0].Split(spearator);
                        string[] choicesEN = newStrEN[0].Split(spearator);

                   
                        for (int j = 1; j < choices.Length; j++)
                        {
                            if(choices[j].Equals("")) continue;
                        
                            Chat.Answer ca = new Chat.Answer();
                            ca.text = choices[j];
                            ca.textEN = choicesEN[j];
                            node.answers.Add(ca);
                        }

                        if (newStr.Length > 1)
                        {
                            string[] Rchoices = newStr[1].Split(spearator);
                            string[] RchoicesEN = newStrEN[1].Split(spearator);
                            for (int j = 0; j < Rchoices.Length; j++)
                            {
                                if (Rchoices[j].Equals("")) continue;

                                Chat.Answer ca = new Chat.Answer();
                                ca.text = Rchoices[j];
                                ca.textEN = RchoicesEN[j];
                                node.answersRight.Add(ca);
                            }
                        }
                    }
                    
                }
                if(node.answers.Count > 0)
                    NodeEditorGUILayout.DynamicPortList("answers", typeof(DialogueBaseNode), serializedObject, NodePort.IO.Output, Node.ConnectionType.Override);
                
                if(node.answersRight.Count > 0)
                    NodeEditorGUILayout.DynamicPortList("answersRight", typeof(DialogueBaseNode), serializedObject, NodePort.IO.Output, Node.ConnectionType.Override);
                                

                
                
            }

            serializedObject.ApplyModifiedProperties();
        }

        public override int GetWidth() {
            return 400;
        }

    }
}