﻿using System.Collections.Generic;
using System.Linq;
using Dialogue;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

namespace DialogueEditor
{
    [CustomNodeEditor(typeof(Dialogue.Wait))]
    public class WaitEditor : NodeEditor
    {
        public override void OnBodyGUI()
        {
            serializedObject.Update();

            Dialogue.Wait node = target as Dialogue.Wait;
            if (NodeEditorWindow.current.zoom > 3)
            {
                serializedObject.ApplyModifiedProperties();
                return;
            }

            NodeEditorGUILayout.PortField(target.GetInputPort("input"), GUILayout.Width(100));
            EditorGUILayout.Space();
            NodeEditorGUILayout.PropertyField(serializedObject.FindProperty("time"));
            NodeEditorGUILayout.PortField(target.GetOutputPort("pass"));

            serializedObject.ApplyModifiedProperties();
        }

        public override int GetWidth()
        {
            return 200;
        }
    }
}