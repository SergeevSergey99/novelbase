﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace Dialogue {
    [NodeTint("#667766")]
    [System.Serializable] public class Chat : DialogueBaseNode {

        //public CharacterInfo character;
        public CharacterInfo WhoTalk;
        [TextArea] public string text;
        [TextArea] public string textEN;
        public string Tags;
        public string OnEnd;
        public string Expressions;
        
        //public double SpeechSpeed = 2.5;
        //public bool isPortretShown = true; 
        [Output(instancePortList = true)] public List<Answer> answers = new List<Answer>();
        [Output(instancePortList = true)] public List<Answer> answersRight = new List<Answer>();

        [System.Serializable] public class Answer {
            public string text;
            public string textEN;

            //public bool isExitDoor = false;
            //public AudioClip voiceClip;
            public Condition[] conditions;

            public bool ConditionsInvoke()
            {
                foreach (var VARIABLE in conditions)
                {
                    if (!VARIABLE.Invoke())
                        return false;
                }

                return true;
            }
        }

        public bool IsAnswerOutputExist(int index)
        {
            NodePort port = null;
            if (answers.Count + answersRight.Count == 0) {
                port = GetOutputPort("output");
            } else {
                if (answers.Count + answersRight.Count <= index) return false;
                if(answers.Count > index)
                    port = GetOutputPort("answers " + index);
                else
                    port = GetOutputPort("answersRight " + (index-answers.Count));
            }

            if (port == null)
            {
                return false;
            }
            if (port.ConnectionCount == 0) return false;
            return true;
        }

        public void AnswerQuestion(int index)
        {
            NodePort port = null;
            if (answers.Count == 0)
            {
                port = GetOutputPort("output");
            }
            else
            {
                if (answers.Count + answersRight.Count <= index) return;
                
                
                if(answers.Count > index)
                    port = GetOutputPort("answers " + index);
                else
                    port = GetOutputPort("answersRight " + (index-answers.Count));
            }

            if (port == null || port.Connection == null) 
            {
                if (graph  != null)
                {
                    (graph as DialogueGraph).current = null;
                    (graph as DialogueGraph).currNode = null;
                }

                return;
            }
            for (int i = 0; i < port.ConnectionCount; i++) {
                NodePort connection = port.GetConnection(i);
                (connection.node as DialogueBaseNode).Trigger();
            }
        }

        public override void Trigger()
        {
            (graph as DialogueGraph).current = this;
            (graph as DialogueGraph).currNode = this;
            (graph as DialogueGraph).indexInLine = 0;
        }
        
    }
}