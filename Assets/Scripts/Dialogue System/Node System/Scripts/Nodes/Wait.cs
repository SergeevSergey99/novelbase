﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace Dialogue
{
    [NodeTint("#44AA55")]
    public class Wait : DialogueBaseNode
    {
        /* public SerializableEvent[]
             trigger = new SerializableEvent[1];// Could use UnityEvent here, but UnityEvent has a bug that prevents it from serializing correctly on custom EditorWindows. So i implemented my own.
        */
        [Output] public DialogueBaseNode pass;
        public float time = 0;

        private DialogueController nc = null;
        private bool wait = false;

        public override void Trigger()
        {
            if (!wait)
            {
                wait = true;
                (graph as DialogueGraph).currNode = this;
                FindObjectOfType<Profile>().DialogueStartWaiting(this, time);
                nc = FindObjectOfType<DialogueController>();
                if (nc.Graph != null && nc.Graph.graph != null && nc.Graph.graph == graph)
                {
                    nc.SetIsInputAwaiting(true);
                }
            }
        }


        public void TriggerNext()
        {
            wait = false;
            //Trigger next nodes
            NodePort port;
            port = GetOutputPort("pass");
            if (port == null)
            {
                return;
            }

            if (port.ConnectionCount == 0)
            {
                (graph as DialogueGraph).current = null;
            }

            for (int i = 0; i < port.ConnectionCount; i++)
            {
                NodePort connection = port.GetConnection(i);

                (connection.node as DialogueBaseNode).Trigger();
                if (nc != null && nc.Graph != null && nc.Graph.graph != null && nc.Graph.graph == graph)
                {
                    if(!((graph as DialogueGraph).currNode is Wait))
                        nc.ContinuetDialog();
                }
            }
        }
    }
}