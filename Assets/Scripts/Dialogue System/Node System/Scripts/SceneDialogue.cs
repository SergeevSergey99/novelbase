﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using XNode;


namespace Dialogue
{
    public class SceneDialogue : SceneGraph
    {
        public void Restart()
        {
            ((DialogueGraph)graph).Restart();
        }

        public void SetAndStartGraph()
        {
            FindObjectOfType<DialogueController>().SetAndStartGraph(this);
        }
        
    }
}