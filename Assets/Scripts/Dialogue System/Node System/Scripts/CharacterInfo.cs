﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dialogue {
	[CreateAssetMenu(menuName = "Graph/Dialogue/CharacterInfo")]
	public class CharacterInfo : ScriptableObject
	{
		[Header("Имя персонажа")]
		public LocalizedString NAME;
		[Header("В творительном падеже")]
		public LocalizedString ByWhoRu;
		
		[Header("Основные данные и флаги необходимые для их открытия")]
		public List<SubDescriptionFlag> Descriptions;

		public Sprite LOGImage;
		public DModel dModel;
	}
}
[System.Serializable]
public class SubDescriptionFlag
{
	public List<Condition> Conditions;
	public LocalizedArea SubDescription;
}