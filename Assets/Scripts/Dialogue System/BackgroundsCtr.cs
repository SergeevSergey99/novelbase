using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundsCtr : MonoBehaviour
{
    [SerializeField] private Backgrounds backgrounds;
    [SerializeField] private Image back;
    [SerializeField] private Image LongBack;

    public void SetBack(string backName)
    {
     
        back.gameObject.SetActive(true);
        LongBack.gameObject.SetActive(false);   
        back.sprite = backgrounds.GetSprite(backName);
    }
    public void SetLongBack(string backName)
    {
        back.gameObject.SetActive(false);
        LongBack.gameObject.SetActive(true);

        LongBack.sprite = backgrounds.GetSprite(backName);
    }

}
