﻿using System;
using System.Collections.Generic;
using Dialogue;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
[Serializable]
public class ComponentData
{
    //public string name;
    public bool enabled;
}
[Serializable]
public class Data
{
    public string scene;
    public string dateTime;
    public string time;
    public int day;

    public string heroName;

    public List<FlagVariable> FlagVariables = new List<FlagVariable>();
    public List<IntVariable> intVariables = new List<IntVariable>();
    public List<LogLine> LogLines = new List<LogLine>();

    public List<SavedCharactersInfo> SavedCharactersInfos = new List<SavedCharactersInfo>();
    public List<string> currentSoundsInSounders = new List<string>();
    public List<ImageObject> ImageObjects = new List<ImageObject>();

    public string GraphPath;
    public int nodeIndex;
    public int DialogueLineIndex;
    
    public int reputation = 0;
    public int DayCounter = 0;
    
    public List<InventoryObjectForSave> Items = new List<InventoryObjectForSave>();
    public List<StringVariable> stringVariables = new List<StringVariable>();

    string addZeroLessTen(int a)
    {
        if (a < 10) return "0" + a;
        return a.ToString();
    }
    public Data()
    {
        reputation = 0;
        DayCounter = 0;
        
        Items = new List<InventoryObjectForSave>();
        stringVariables = new List<StringVariable>();
    }
    public Data(Profile profile)
    {
        scene = SceneManager.GetActiveScene().name;
        var dt = DateTime.Now;
        //dateTime = DateTime.Now.ToString("MM/dd/yyyy HH:MM:ss");
        dateTime = addZeroLessTen(dt.Day) + "/" + addZeroLessTen(dt.Month) + "/" + addZeroLessTen(dt.Year)
                   + " " + addZeroLessTen(dt.Hour) + ":" + addZeroLessTen(dt.Minute) + ":" + addZeroLessTen(dt.Second);
        time = profile._time;
        day = profile._day;
        heroName = profile.heroName;

        string debugStr = "";

        FlagVariables = new List<FlagVariable>(profile.FlagVariables);
        intVariables = new List<IntVariable>(profile.intVariables);
        
        LogLines = new List<LogLine>(profile.LogPhrases);

        GameObject.Find("MANAGER").transform.Find("DIALOGUE INPUT").GetComponent<DialogueInput>().SetRemembered();
        
        debugStr += "===================== FlagVariables =====================\n";
        foreach (var VARIABLE in FlagVariables) debugStr += VARIABLE.name + " = " + VARIABLE.isRaised + "\n";
        debugStr += "===================== intVariables =====================\n";
        foreach (var VARIABLE in intVariables) debugStr += VARIABLE.name + " = " + VARIABLE.value + "\n";
        debugStr += "===================== Characters =====================\n";
        foreach (var VARIABLE in GameObject.Find("MANAGER").transform.Find("CHARACTERS").GetComponent<DCharacters>().GetModelsInSceneDic())
        {
            if(VARIABLE.Value == null) continue;
            if(VARIABLE.Value == null) continue;
            if(!VARIABLE.Value.gameObject.activeSelf) continue;
            if(VARIABLE.Value.GetOpacity() < 0.01f) continue;
            SavedCharactersInfo sci;
            sci.name = VARIABLE.Key;
            sci.opacity = VARIABLE.Value.GetModelOpacity();
            sci.currentExpression = VARIABLE.Value.GetCurrentExpression();
            sci.isFocused = VARIABLE.Value.GetFocused();

            debugStr += sci.name + "\n";
            Vector3 v = VARIABLE.Value.transform.localPosition;
            sci.pos_X = v.x;
            sci.pos_Y = v.y;
            sci.pos_Z = v.z;
            debugStr += "pos = " + v + "\n";
            
            v = VARIABLE.Value.transform.GetChild(0).localPosition;
            sci.child_pos_X = v.x;
            sci.child_pos_Y = v.y;
            sci.child_pos_Z = v.z;
            debugStr += "childpos = " + v + "\n";

            v = VARIABLE.Value.transform.localScale;
            sci.scale_X = v.x;
            sci.scale_Y = v.y;
            sci.scale_Z = v.z;
            debugStr += "scale = " + v + "\n";
            
            v = VARIABLE.Value.GetComponent<DModel>().GetScale();
            sci.child_scale_X = v.x;
            sci.child_scale_Y = v.y;
            sci.child_scale_Z = v.z;
            debugStr += "childpos = " + v + "\n";

            var c = VARIABLE.Value.GetColor();
            sci.colorA = c.a;
            sci.colorR = c.r;
            sci.colorG = c.g;
            sci.colorB = c.b;
            debugStr += "color = " + c + "\n";
            
            SavedCharactersInfos.Add(sci);
        }

        foreach (var VARIABLE in GameObject.Find("MANAGER").transform.Find("COMMANDS").GetComponent<DialogueCommands>()
            .dSoundManager)
        {
            if (VARIABLE == null || VARIABLE.GetComponent<AudioSource>() == null ||
                VARIABLE.GetComponent<AudioSource>().clip == null) currentSoundsInSounders.Add("");
            else currentSoundsInSounders.Add(VARIABLE.GetComponent<AudioSource>().clip.name);
        }

        nodeIndex = -1;
        DialogueLineIndex = 0;
        SceneDialogue sgraph = GameObject.Find("MANAGER").transform.Find("DIALOGUE CORE").GetComponent<DialogueController>().Graph;
        if (sgraph != null)
        {
            GameObject graph = sgraph.gameObject;
            GraphPath = "";
            if (graph != null)
            {
                nodeIndex = ((DialogueGraph)graph.GetComponent<SceneDialogue>().graph).GetCurrentChatIndex();
                DialogueLineIndex = ((DialogueGraph)graph.GetComponent<SceneDialogue>().graph).indexInLine;

                if (profile.gameList.GetComponent<PrefabGraphs>().graphsList.Contains(sgraph))
                    GraphPath = graph.name;
                else
                {
                    do
                    {
                        GraphPath = "/" + graph.name + GraphPath;
                        if (graph.transform.parent == null) break;
                        graph = graph.transform.parent.gameObject;
                    } while (graph.transform != null);
                }
            }
        }
        else
        {
            GraphPath = "/";
        }

        debugStr += "GraphPath: " + GraphPath + "\n";

        Debug.Log(debugStr);
        List<string> gos = new List<string>(0);
        /*
         gos.Add("BACKGROUNDS_CANVAS");
        //gos.Add("DIALOG UI");
        gos.Add("TOOLSCanvas/CHOICES");
        gos.Add("EXTRA_PLAINS");
        gos.Add("Overlay");
        gos.Add("ExtraCanvas1");
        gos.Add("ExtraCanvas2");
        gos.Add("ExtraCanvas3");
        gos.Add("ExtraCanvas4");
        gos.Add("ExtraCanvas5");
        */
        gos.Add("DIALOGUE_SCENE/BACKGROUNDS_CANVAS");
        gos.Add("DIALOGUE_SCENE/TOOLSCanvas/CHOICES");
        gos.Add("DIALOGUE_SCENE/TOOLSCanvas/CHOICES_Right");
        gos.Add("DIALOGUE_SCENE/EXTRA_PLAINS");
        gos.Add("DIALOGUE_SCENE/Overlay");
        gos.Add("DIALOGUE_SCENE/ExtraCanvas1");
        gos.Add("DIALOGUE_SCENE/ExtraCanvas2");
        gos.Add("DIALOGUE_SCENE/ExtraCanvas3");
        gos.Add("DIALOGUE_SCENE/ExtraCanvas4");
        gos.Add("DIALOGUE_SCENE/ExtraCanvas5");
        gos.Add("DIALOGUE_SCENE/KeyHoleCanvas");
        gos.Add("DIALOGUE_SCENE/NavCanvas");
        foreach (var Folder in gos)
        {
            GenerateFolders(Folder);
        }
        
        DayCounter = profile.DayCounter;

        debugStr += "--------------------------------------\n";
        debugStr += "reputation = " + reputation +"\n";
        debugStr += "DayCounter = " + DayCounter +"\n";
        debugStr += "===================== Items =====================\n";
        
        Items = new List<InventoryObjectForSave>();
        foreach (var item in profile.Items)
        {
            InventoryObjectForSave tmp = new InventoryObjectForSave();
            tmp.CodeName = item.inventoryObjectScriptable.name;
            tmp.cnt = item.count;
            Items.Add(tmp);
            debugStr +=  item.inventoryObjectScriptable.name + "\n";
        }
        
        stringVariables = new List<StringVariable>(profile.stringVariables);
        
        debugStr += "===================== stringVariables =====================\n";
        foreach (var VARIABLE in stringVariables) debugStr += VARIABLE.name + " = " + VARIABLE.value + "\n";
        
        
        Debug.Log(debugStr);
    }


    void GenerateFolders(string folder)
    {
        var go = GameObject.Find(folder);
        if(go == null) go = GameObject.Find(folder.Replace("DIALOGUE_SCENE/",""));;
        if(go == null) return;
        AddToFolder(folder, go.transform);
    }

    void AddToFolder(string path, Transform Folder)
    {
        if(Folder.childCount == 0) return;
        
        foreach (Transform VARIABLE in Folder)
        {
            if(VARIABLE.name.StartsWith("TMP ")) return;
            if(VARIABLE.name.StartsWith("Parameters")|| VARIABLE.name.StartsWith("Parts")|| VARIABLE.name.StartsWith("Drawables")) return;
            AddToFolder(path +"/"+ VARIABLE.name, VARIABLE);
            
            ImageObject io = new ImageObject();

            io.Folder = path + "/" + VARIABLE.name;
            io.ObjectInScene = VARIABLE.name;
            io.imageName =
                VARIABLE.GetComponent<Image>() == null || VARIABLE.GetComponent<Image>().sprite == null
                    ? ""
                    : VARIABLE.GetComponent<Image>().sprite.name
                ;
            
            if (VARIABLE.GetComponent<Image>() != null)
            {
                io.colorA = VARIABLE.GetComponent<Image>().color.a;
                io.colorR = VARIABLE.GetComponent<Image>().color.r;
                io.colorG = VARIABLE.GetComponent<Image>().color.g;
                io.colorB = VARIABLE.GetComponent<Image>().color.b;
            }
            else
            {
                io.colorA = 1;
                io.colorR = 1;
                io.colorG = 1;
                io.colorB = 1;
            }

            io.ObjectTag = VARIABLE.tag;
            io.isActive = VARIABLE.gameObject.activeSelf;
            if (VARIABLE.GetComponent<CanvasGroup>() == null)
            {
                io.CG_alpha = -1;
                io.CG_block = false;
                io.CG_interactable = false;
                io.CG_ignore = false;
            }
            else
            {
                io.CG_alpha = VARIABLE.GetComponent<CanvasGroup>().alpha;
                io.CG_block = VARIABLE.GetComponent<CanvasGroup>().blocksRaycasts;
                io.CG_interactable = VARIABLE.GetComponent<CanvasGroup>().interactable;
                io.CG_ignore = VARIABLE.GetComponent<CanvasGroup>().ignoreParentGroups;

                if (VARIABLE.GetComponent<SimpleAnimations>() != null)
                {
                    io.CG_alpha = VARIABLE.GetComponent<SimpleAnimations>().nextAlpha;
                    io.CG_block = VARIABLE.GetComponent<SimpleAnimations>().nextBlockRaycast;
                    io.CG_interactable = VARIABLE.GetComponent<SimpleAnimations>().nextInteractable;
                }
            }

            MonoBehaviour[] components = VARIABLE.GetComponents<MonoBehaviour>();
            io.componentsData = new List<ComponentData>();
            foreach (var component in components)
            {
                ComponentData cmp = new ComponentData();
                //cmp.name = component.name;
                cmp.enabled = component.enabled;
                io.componentsData.Add(cmp);
            }

            


            ImageObjects.Add(io);
        }
    }
    
}

[Serializable]
public class ImageObject
{
    
     
    public string Folder;
    public string ObjectInScene;
    public string ObjectTag;

    public string imageName;
    public bool isActive;

    public float CG_alpha;
    public bool CG_interactable;
    public bool CG_block;
    public bool CG_ignore;
    public float colorR;
    public float colorG;
    public float colorB;
    public float colorA;

    
    public List<ComponentData> componentsData;

    public ImageObject()
    {
        colorR = 1;
        colorG = 1;
        colorB = 1;
        colorA = 1;

    }
    public ImageObject(ImageObject imageObject)
    {
        Folder = imageObject.Folder;
        ObjectInScene = imageObject.ObjectInScene;
        ObjectTag = imageObject.ObjectTag;
        imageName = imageObject.imageName;
        isActive = imageObject.isActive;
        CG_alpha = imageObject.CG_alpha;
        CG_interactable = imageObject.CG_interactable;
        CG_block = imageObject.CG_block;
        CG_ignore = imageObject.CG_ignore;
        componentsData = new List<ComponentData>(imageObject.componentsData);
        
        colorR = imageObject.colorR;
        colorG = imageObject.colorG;
        colorB = imageObject.colorB;
        colorA = imageObject.colorA;

    }

}

[Serializable]
public struct SavedCharactersInfo
{
    public string name;
    public string currentExpression;
    public float opacity;

    public float pos_X;
    public float pos_Y;
    public float pos_Z;

    public float scale_X;
    public float scale_Y;
    public float scale_Z;
    
    public float child_pos_X;
    public float child_pos_Y;
    public float child_pos_Z;

    public float child_scale_X;
    public float child_scale_Y;
    public float child_scale_Z;

    public float colorR;
    public float colorG;
    public float colorB;
    public float colorA;

    public bool isFocused;
}

[System.Serializable]
public class InventoryObjectForSave
{
    public string CodeName;
    public int cnt;
}