using System;
using System.Collections;
using System.Collections.Generic;
using Dialogue;
using UnityEngine;

[CreateAssetMenu(menuName = "GameList/InventoryObject")]
public class InventoryObjectScriptable : ScriptableObject
{
    public Sprite Image;
    
    [Header("Название предмета")]
    public LocalizedString Name;
    [Header("Описание предмета")]
    public LocalizedArea Description;
    
    [Header("Взаимодействие с предметом")]
    public LocalizedString interactName;
    public string InteractTags;
    public SceneDialogue InteractGraph;

    public bool isCountable = false;

}