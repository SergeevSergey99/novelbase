using System;
using System.Collections;
using System.Collections.Generic;
using Live2D.Cubism.Core;
using Live2D.Cubism.Rendering;
using UnityEngine;
using Random = System.Random;

[RequireComponent(typeof(ModelMove))]
[RequireComponent(typeof(ModelMirror))]
public class DModel : MonoBehaviour
{
    //[SerializeField] private ScaleInOut scale;
    //[SerializeField] private FadeInOut fade;
    //[SerializeField] private MoveInOut move;
    //[SerializeField] private ModelMove modelMove;
    //[SerializeField] private ModelMirror modelMirror;
    //[SerializeField] private ModelExpressions expressions;


    private void Awake()
    {
        if (transform.childCount > 0 && model == null)
        {
            model = transform.GetChild(0).gameObject;
            controller = model.GetComponent<CubismRenderController>();
            xStart = model.transform.localPosition.x;
            yStart = model.transform.localPosition.y;
            zStart = model.transform.localPosition.z;
            baseScale = model.transform.localScale.x;
            _model = model.FindCubismModel();

            foreach (var VARIABLE in parameters)
            {
                if (!VARIABLE.name.Equals(""))
                {
                    var pars = model.transform.Find("Parameters");
                    if (pars == null) continue;
                    var par = pars.Find(VARIABLE.name);
                    if (par == null) continue;
                    VARIABLE.index = par.GetSiblingIndex();
                }
            }
        }
    }

    private float baseScale;
    [SerializeField] private float Fade_animTime = 0.4f;
    public float GetFade_animTime() => Fade_animTime;
    private CubismRenderController controller;
    private float current = 0;

    private int fade_id;


    private GameObject model;
    public float xOffset = 1f;
    [SerializeField] private float Appear_animTime = .4f;
    private float xStart;
    private float yStart;
    private float zStart;
    private int Appear_id;

    [Serializable]
    public class ParameterToUpdate
    {
        public int index;
        public string name = "";
        public float value;
        public float speedKoef = 1;

        public ParameterState state = ParameterState.CONSTANT;

        //[NonSerialized]
        public float lastValue;

        [NonSerialized] public float time;

        //[NonSerialized]
        public float ampl;

        public enum ParameterState
        {
            CONSTANT,
            LERP,
            SIN,
            COS
        }
    }

    [SerializeField] private List<ParameterToUpdate> parameters;

    private CubismModel _model;

    private void LateUpdate()
    {
        foreach (var VARIABLE in parameters)
        {
            if (VARIABLE.index >= _model.Parameters.Length) continue;
            if (VARIABLE.value > _model.Parameters[VARIABLE.index].MaximumValue)
                VARIABLE.value = _model.Parameters[VARIABLE.index].MaximumValue;
            if (VARIABLE.value < _model.Parameters[VARIABLE.index].MinimumValue)
                VARIABLE.value = _model.Parameters[VARIABLE.index].MinimumValue;

            if (VARIABLE.state == ParameterToUpdate.ParameterState.CONSTANT)
                _model.Parameters[VARIABLE.index].Value = VARIABLE.value;
            else if (VARIABLE.state == ParameterToUpdate.ParameterState.LERP)
            {
                if (Mathf.Abs(VARIABLE.lastValue - VARIABLE.value) <= 0.01f)
                {
                    _model.Parameters[VARIABLE.index].Value = VARIABLE.value;
                    VARIABLE.lastValue = VARIABLE.value;
                    VARIABLE.state = ParameterToUpdate.ParameterState.CONSTANT;
                    continue;
                }

                VARIABLE.lastValue = Mathf.Lerp(
                    VARIABLE.lastValue, VARIABLE.value, VARIABLE.speedKoef * Time.deltaTime);
                _model.Parameters[VARIABLE.index].Value = VARIABLE.lastValue;
            }
            else if (VARIABLE.state == ParameterToUpdate.ParameterState.SIN)
            {
                VARIABLE.time += Time.deltaTime;
                float ampl =
                    (_model.Parameters[VARIABLE.index].MaximumValue - _model.Parameters[VARIABLE.index].MinimumValue) *
                    VARIABLE.ampl;
                VARIABLE.lastValue = Mathf.Sin(VARIABLE.time * VARIABLE.speedKoef) * ampl + VARIABLE.value;
                _model.Parameters[VARIABLE.index].Value = VARIABLE.lastValue;
            }
            else if (VARIABLE.state == ParameterToUpdate.ParameterState.COS)
            {
                VARIABLE.time += Time.deltaTime;
                float ampl =
                    (_model.Parameters[VARIABLE.index].MaximumValue - _model.Parameters[VARIABLE.index].MinimumValue) *
                    VARIABLE.ampl;
                VARIABLE.lastValue = Mathf.Cos(VARIABLE.time * VARIABLE.speedKoef) * ampl + VARIABLE.value;
                _model.Parameters[VARIABLE.index].Value = VARIABLE.lastValue;
            }
        }
    }

    public void SetParamCONST(string parametrs)
    {
        var param = parametrs.Split(',');
        if (param.Length > 1)
        {
            SetParamCONST(Int32.Parse(param[0]), float.Parse(param[1].Replace('.', ',')));
        }
    }

    public void SetParamLERP(string parametrs)
    {
        var param = parametrs.Split(',');
        if (param.Length == 2)
        {
            SetParamLERP(Int32.Parse(param[0]), float.Parse(param[1].Replace('.', ',')));
        }
        else if (param.Length == 3)
        {
            SetParamLERP(
                Int32.Parse(param[0]),
                float.Parse(param[1].Replace('.', ',')),
                float.Parse(param[2].Replace('.', ',')));
        }
    }

    public void SetParamSIN(string parametrs)
    {
        var param = parametrs.Split(',');
        if (param.Length == 2)
        {
            SetParamSIN(Int32.Parse(param[0]), float.Parse(param[1].Replace('.', ',')));
        }
    }

    public void SetParamCOS(string parametrs)
    {
        var param = parametrs.Split(',');
        if (param.Length > 1)
        {
            SetParamCOS(Int32.Parse(param[0]), float.Parse(param[1].Replace('.', ',')));
        }
    }

    public void RemoveParameter(int num)
    {
        var p = parameters.FindIndex(i => i.index == num);
        if (p != -1)
            parameters.RemoveAt(p);
    }

    public void RemoveParameterAtIndex(int index)
    {
        if (index >= 0 && index < parameters.Count)
            parameters.RemoveAt(index);
    }

    public void SetParamCONST(int index, float value)
    {
        var p = parameters.Find(i => i.index == index);
        if (p == null)
        {
            p = new ParameterToUpdate();
            p.index = index;
            p.name = "";
            p.value = value;
            p.lastValue = value;
            p.speedKoef = 1;
            p.state = ParameterToUpdate.ParameterState.CONSTANT;
            parameters.Add(p);
        }
        else
        {
            p.state = ParameterToUpdate.ParameterState.CONSTANT;
            p.value = value;
            p.lastValue = value;
            p.lastValue = value;
        }
    }

    public void SetParamLERP(int index, float value)
    {
        var p = parameters.Find(i => i.index == index);
        if (p == null)
        {
            p = new ParameterToUpdate();
            p.index = index;
            p.name = "";
            p.value = value;
            p.lastValue = _model.Parameters[index].Value;
            p.speedKoef = 1;
            p.state = ParameterToUpdate.ParameterState.LERP;
            p.time = 0;
            parameters.Add(p);
        }
        else
        {
            p.state = ParameterToUpdate.ParameterState.LERP;
            p.value = value;
            p.time = 0;
        }
    }

    public void SetParamLERP(int index, float value, float speed)
    {
        var p = parameters.Find(i => i.index == index);
        if (p == null)
        {
            p = new ParameterToUpdate();
            p.index = index;
            p.name = "";
            p.value = value;
            p.lastValue = _model.Parameters[index].Value;
            p.speedKoef = speed;
            p.state = ParameterToUpdate.ParameterState.LERP;
            p.time = 0;
            parameters.Add(p);
        }
        else
        {
            p.state = ParameterToUpdate.ParameterState.LERP;
            p.value = value;
            p.speedKoef = speed;
            p.time = 0;
        }
    }

    public void SetParamSIN(int index, float amplitude)
    {
        var p = parameters.Find(i => i.index == index);
        if (p == null)
        {
            p = new ParameterToUpdate();
            p.index = index;
            p.name = "";
            p.ampl = amplitude;
            p.value = _model.Parameters[p.index].Value;
            p.lastValue = _model.Parameters[p.index].Value;
            p.speedKoef = 1;
            p.state = ParameterToUpdate.ParameterState.SIN;
            p.time = 0;
            parameters.Add(p);
        }
        else
        {
            p.state = ParameterToUpdate.ParameterState.SIN;
            p.value = _model.Parameters[p.index].Value;
            p.ampl = amplitude;
            p.time = 0;
        }
    }

    public void SetParamCOS(int index, float amplitude)
    {
        var p = parameters.Find(i => i.index == index);
        if (p == null)
        {
            p = new ParameterToUpdate();
            p.index = index;
            p.name = "";
            p.ampl = amplitude;
            p.value = _model.Parameters[p.index].Value;
            p.lastValue = _model.Parameters[p.index].Value;
            p.speedKoef = 1;
            p.state = ParameterToUpdate.ParameterState.COS;
            p.time = 0;
            parameters.Add(p);
        }
        else
        {
            p.state = ParameterToUpdate.ParameterState.COS;
            p.value = _model.Parameters[p.index].Value;
            p.ampl = amplitude;
            p.time = 0;
        }
    }

    //public void FadeIn() => fade.FadeIn();
    public void FadeIn()
    {
        if (model == null) Awake();
        if (model == null) return;
        LeanTween.cancel(fade_id);
        model.SetActive(true);
        //LeanTween.moveLocalX(model, 0, 0);
        fade_id = LeanTween.value(gameObject, current, 1, Fade_animTime).setEaseInQuad().setOnUpdate((float val) =>
        {
            controller.Opacity = val;
            current = val;
        }).id;
    }

    //public void FadeOut() => fade.FadeOut();
    public void FadeOut()
    {
        LeanTween.cancel(fade_id);
        fade_id = LeanTween.value(gameObject, current, 0, Fade_animTime).setEaseOutQuad().setOnUpdate((float val) =>
        {
            controller.Opacity = val;
            current = val;
        }).id;
        StartCoroutine(setActiveAfter(false, Fade_animTime, fade_id));
    }

    public void FadeOut(float _time)
    {
        LeanTween.cancel(fade_id);
        fade_id = LeanTween.value(gameObject, current, 0, _time).setEaseOutQuad().setOnUpdate((float val) =>
        {
            controller.Opacity = val;
            current = val;
        }).id;
        StartCoroutine(setActiveAfter(false, _time, fade_id));
    }

    //public void FadeOut() => fade.FadeOut();
    public void FadeOutAndDeacrive()
    {
        LeanTween.cancel(fade_id);
        fade_id = LeanTween.value(gameObject, current, 0, Fade_animTime).setEaseOutQuad().setOnUpdate((float val) =>
        {
            controller.Opacity = val;
            current = val;
        }).id;
        model.SetActive(false);
    }

    public void HideModel()
    {
        controller.Opacity = 0;
        current = 0;
        //LeanTween.value(gameObject, current, 0, 0).setEaseOutQuad().setOnUpdate((float val) => { controller.Opacity = val; current = val; });
        model.SetActive(false);
        //StartCoroutine(setActiveAfter(false, 0.0001f));
    }

    IEnumerator setActiveAfter(bool isActive, float time, int id)
    {
        yield return new WaitForSeconds(time);

        if (model == null) Awake();
        if (model != null && fade_id == id)
            model.SetActive(isActive);
    }

    public float GetOpacity() => current;

    //public void AppearIn(int multi) => move.MoveIn(multi);
    public void AppearIn(float multi)
    {
        if (model == null) Awake();
        if (model == null) return;

        LeanTween.cancel(Appear_id);
        LeanTween.moveLocalX(model, model.transform.localPosition.x - xOffset * multi, 0);
        Appear_id = LeanTween.moveLocalX(model, xStart, Appear_animTime).setEaseInOutQuad().id;
    }

    //public void AppearOut(int multi) => move.MoveOut(multi);
    public void AppearOut(float multi)
    {
        if (model == null) Awake();
        if (model == null) return;

        LeanTween.cancel(Appear_id);
        Appear_id = LeanTween.moveLocalX(model, model.transform.localPosition.x - xOffset * multi, Appear_animTime)
            .setOnComplete(this.SetToZero).setEaseInOutQuad().id;
    }

    public void SetColor(float r, float g, float b, float a = 1)
    {
        if (model == null) Awake();
        if (model == null) return;
        model.GetComponent<CubismRenderController>().modelColor = new Color(r, g, b, a);
    }

    public Color GetColor()
    {
        if (model == null) Awake();
        return model.GetComponent<CubismRenderController>().modelColor;
       
    }

    private void SetToZero()
    {
        if (model == null) Awake();
        if (model == null) return;
        model.transform.localPosition = new Vector3(xStart, yStart, zStart);
    }

    public void MoveTo(Vector3 pos) => GetComponent<ModelMove>().MoveTo(pos);
    public void FocusMoveTo(Vector3 pos) => GetComponent<ModelMove>().FocusMoveTo(pos);
    public void MoveToX(float x) => GetComponent<ModelMove>().MoveToX(x);

    public void SetTo(Vector3 pos) => GetComponent<ModelMove>().SetTo(pos);

    public void MirrorTo(float val) => GetComponent<ModelMirror>().MirrorTo(val);

    public void SetDefaultZ(float z) => GetComponent<ModelMove>().SetDefaultZ(z);

    public void MoveToZ(float z) => GetComponent<ModelMove>().MoveToZ(z);

    public void ReturnToDefaultZ() => GetComponent<ModelMove>().ReturnToDefaultZ();


    [SerializeField] private float Scale_animTime = .1f;
    [SerializeField] private float Focus_Scale_animTime = .5f;
    [SerializeField] private float smallScale = 1.05f, normalScale = 1, bigScale = 1.3f;
    private int Scale_id;
    private bool isFocused = false;


    public void setFocused(bool Focused)
    {
        if (model == null) Awake();
        if (model == null) return;
        isFocused = Focused;
    }

    public GameObject GetModel()
    {
        if (model == null) Awake();
        return model;
    }

    public bool GetFocused()
    {
        return isFocused;
    }

    //public void ScaleBig() => scale.ScaleInBig();
    public void ScaleBig()
    {
        if (model == null) Awake();
        if (model == null) return;

        isFocused = true;
        LeanTween.cancel(Scale_id);
        Scale_id = LeanTween.scale(model, Vector3.one * bigScale * baseScale, Scale_animTime).id;
    }

    public void FocusScaleBig()
    {
        if (model == null) Awake();
        if (model == null) return;

        isFocused = true;
        LeanTween.cancel(Scale_id);
        Scale_id = LeanTween.scale(model, Vector3.one * bigScale * baseScale, Focus_Scale_animTime).id;
    }

    //public void ScaleSmall() => scale.ScaleInSmall();
    public void ScaleSmall()
    {
        if (model == null) Awake();
        if (model == null) return;

        if (isFocused) return;
        LeanTween.cancel(Scale_id);
        Scale_id = LeanTween.scale(model, Vector3.one * smallScale * baseScale, Scale_animTime).id;
    }

    //public void ScaleNormal() => scale.ScaleOut();
    public void ScaleNormal()
    {
        if (model == null) Awake();
        if (model == null) return;

        if (isFocused) return;
        LeanTween.cancel(Scale_id);
        Scale_id = LeanTween.scale(model, Vector3.one * normalScale * baseScale, Scale_animTime).id;
    }

    //public void Unfocus() => scale.Unfocus();
    public void UnfocusMove()
    {
        isFocused = false;
        ScaleNormalMove();
    }

    public void ScaleNormalMove()
    {
        if (model == null) Awake();
        if (model == null) return;

        if (isFocused) return;
        LeanTween.cancel(Scale_id);
        Scale_id = LeanTween.scale(model, Vector3.one * normalScale * baseScale, Focus_Scale_animTime).id;
    }

    //public void Unfocus() => scale.Unfocus();
    public void Unfocus()
    {
        isFocused = false;
        ScaleNormal();
    }

    //public void SetExpression(string expression) => expressions.SetExpression(expression);
    public void SetExpression(string expression)
    {
        lastExpression = expression;
        transform.GetChild(0).GetComponent<Animator>().SetTrigger(expression);
    }

    private string lastExpression = "";

    public string GetLastExpression() => lastExpression;

    //public void SetAnimatorBool(string expression, bool b) => expressions.SetAnimatorBool(expression, b);
    public void SetAnimatorBool(string expression, bool b)
    {
        transform.GetChild(0).GetComponent<Animator>().SetBool(expression, b);
    }


    public float GetModelOpacity()
    {
        if (model == null) Awake();
        if (model == null) return 0;
        return model.GetComponent<CubismRenderController>().Opacity;
    }

    public void SetModelOpacity(float opacity)
    {
        if (model == null) Awake();
        if (model == null) return;
        if (opacity <= 0.1f)
        {
            opacity += (new Random().Next() % 1000 + 1) / 1000000.0f;
            model.SetActive(false);
            //StartCoroutine(setActiveAfter(false, 0));
        }
        else
        {
//            if(model.activeInHierarchy == true)
//                StartCoroutine(setActiveAfter(true, 0));

            model.SetActive(true);
        }

        model.GetComponent<CubismRenderController>().Opacity = opacity;
    }


    public string GetCurrentExpression()
    {
        if (model == null) Awake();
        if (model == null) return "";
        if (!transform.parent.gameObject.activeSelf || !transform.parent.gameObject.activeInHierarchy ||
            !gameObject.activeSelf || !model.activeSelf) return "";
        if (model.GetComponent<Animator>() != null && model.GetComponent<Animator>().layerCount > 0 &&
            model.GetComponent<Animator>().GetCurrentAnimatorClipInfoCount(0) > 0)
            return model.GetComponent<Animator>().GetCurrentAnimatorClipInfo(0)[0].clip.name;
        return "";
    }

    public void SetCurrentExpression(string str)
    {
        if (model == null) Awake();
        if (model == null) return;
        model.GetComponent<Animator>().Play(str);
    }


    public Vector3 GetScale()
    {
        return model.transform.localScale;
    }

    public void SetScale(Vector3 v)
    {
        if (model == null) Awake();
        if (model == null) return;
        model.transform.localScale = v;
    }
}