using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelMove : MonoBehaviour
{
    [SerializeField] private float animTime = .1f;
    private float FocusMoveToanimTime = .1f;
    private int id, id2, id3;
    private float defaultZ;

    public float GetAnimTime() => animTime;
    bool isAnimated()
    {
        return LeanTween.isTweening(id) || LeanTween.isTweening(id2) || LeanTween.isTweening(id3);
    }
    
    public void MoveTo(Vector3 pos)
    {
        // pos = new Vector3(pos.x, pos.y, transform.position.z);
        LeanTween.cancel(id);
        LeanTween.cancel(id2);
        id = LeanTween.moveX(gameObject, pos.x, animTime).setEaseInOutQuad().id;
        id2 = LeanTween.moveY(gameObject, pos.y, animTime).setEaseInOutQuad().id;
    }
    public void FocusMoveTo(Vector3 pos)
    {
        // pos = new Vector3(pos.x, pos.y, transform.position.z);
        LeanTween.cancel(id);
        LeanTween.cancel(id2);
        id = LeanTween.moveX(gameObject, pos.x, FocusMoveToanimTime).setEaseInOutQuad().id;
        id2 = LeanTween.moveY(gameObject, pos.y, FocusMoveToanimTime).setEaseInOutQuad().id;
    }
    public void MoveToX(float pos)
    {
        // pos = new Vector3(pos.x, pos.y, transform.position.z);
        LeanTween.cancel(id);
        id = LeanTween.moveX(gameObject, pos, animTime).setEaseInOutQuad().id;
    }
    public void MoveToLocalX(float pos)
    {
        // pos = new Vector3(pos.x, pos.y, transform.position.z);
        LeanTween.cancel(id);
        id = LeanTween.moveLocalX(gameObject, transform.localPosition.x + pos, animTime).setEaseInOutQuad().id;
    }
    public void MoveToY(float pos)
    {
        // pos = new Vector3(pos.x, pos.y, transform.position.z);
        LeanTween.cancel(id2);
        id2 = LeanTween.moveY(gameObject, pos, animTime).setEaseInOutQuad().id;
    }
    public void ScaleTo(float newScale)
    {
        // pos = new Vector3(pos.x, pos.y, transform.position.z);
        LeanTween.cancel(id3);
        id3 = LeanTween.scale(gameObject, Vector3.one * newScale, animTime).setEaseInOutQuad().id;
    }

    private float baseScale = 1;
    private void Awake()
    {
        baseScale = transform.localScale.x;
    }

    public void ScaleLocalTo(float newScale)
    {
        // pos = new Vector3(pos.x, pos.y, transform.position.z);
        LeanTween.cancel(id3);
        id3 = LeanTween.scale(gameObject, Vector3.one * baseScale * newScale, animTime).setEaseInOutQuad().id;
    }

    public void SetTo(Vector3 pos)
    {
        // pos = new Vector3(pos.x, pos.y, transform.position.z);
        LeanTween.cancel(id);
        LeanTween.cancel(id2);
        LeanTween.moveX(gameObject, pos.x, 0);
        LeanTween.moveY(gameObject, pos.y, 0);
    }

    public void MoveToZ(float z)
    {
        //LeanTween.cancel(id2);
        LeanTween.moveZ(gameObject, z, 0);
    }

    public void SetDefaultZ(float z)
    {
        defaultZ = z;
        MoveToZ(z);
    }

    public void ReturnToDefaultZ()
    {
        MoveToZ(defaultZ);
    }
}
