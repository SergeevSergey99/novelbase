using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelMirror : MonoBehaviour
{
    public void MirrorTo(float val)
    {
        LeanTween.scaleX(gameObject, val*Mathf.Abs(transform.localScale.x), 0);
    }
}
